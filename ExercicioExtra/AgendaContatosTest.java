import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class AgendaContatosTest{
    @Test
    public void adicionarContatoEPesquisar(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Evelin", "123123123");
        agenda.adicionar("dbc", "1212121");
        assertEquals("123123123", agenda.consultar("Evelin"));
    }
    
    @Test
    public void adicionarContatoEPesquisarPorTelefone(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Evelin", "123123123");
        agenda.adicionar("dbc", "1212121");
        assertEquals("dbc", agenda.consultarNome("1212121"));
    }
    
    @Test
    public void adicionarContatoEGerarCSV(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionar("Evelin","123123123");
        agenda.adicionar("dbc","1212121");
        String separador = System.lineSeparator();
        String esperado = String.format("Evelin,123123123%sdbc,1212121%s",separador,separador);
        assertEquals(esperado, agenda.csv());
    }
}
