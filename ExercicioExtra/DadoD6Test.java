import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DadoD6Test{
    @Test
    public void testarSorteio(){
     DadoD6 random = new DadoD6();
     random.gerarNumeroAleatorio();
     assertEquals(1,random.getNumero());
    }
}
