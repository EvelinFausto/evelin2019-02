import java.util.Random;

public class DadoD6{
    private int numero;
    private Random random = new Random();
    
    public void gerarNumeroAleatorio(){
        this.numero = random.nextInt(6)+1;
    }
    
    public int getNumero(){
        return this.numero;
    }
}
