package br.com.dbccompany.cartoes.DAO;

import br.com.dbccompany.cartoes.DTO.LojasDTO;
import br.com.dbccompany.cartoes.Entity.Lojas;

public class LojasDAO extends AbstractDAO<Lojas>{

	public Lojas parseFrom(LojasDTO dto) {
		Lojas lojas = null;
		if(dto.getIdLoja() != null) {
			lojas = buscar(1);
		}else {
			
		lojas.setId(1);
		
		}
		lojas.setNome("bar");
		criar(lojas);
		return lojas;
	}
	
	@Override
	protected Class<Lojas> getEntityClass(){
		return Lojas.class;
	}
}
