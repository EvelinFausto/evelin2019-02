package br.com.dbccompany.cartoes.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator( allocationSize = 1, name = "LANCAMENTOS_SEQ", sequenceName = "LANCAMENTOS_SEQ")
public class Lancamentos extends AbstractEntity{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue( generator = "LANCAMENTOS_SEQ", strategy = GenerationType.SEQUENCE )
	@Column(name = "ID_LANCAMENTO")
	private Integer id;
	
	@Column( name = "descricao", length = 100, nullable = false)
	private String descricao;
	
	@Column( name = "valor", length = 100, nullable = false)
	private Double valor;
	
	@Column( name = "dataCompra", length = 100, nullable = false)
	private String dataCompra;
	
	@ManyToOne
	@JoinColumn( name = "fk_id_cartao" )
	private Cartoes cartaoLancamento;
	
	@ManyToOne
	@JoinColumn( name = "fk_id_loja" )
	private Lojas lancamentoLoja;
	
	@ManyToOne
	@JoinColumn( name = "fk_id_emissor" )
	private Emissores lancamentoEmissor;

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public String getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(String dataCompra) {
		this.dataCompra = dataCompra;
	}

	public Cartoes getCartaoLancamento() {
		return cartaoLancamento;
	}

	public void setCartaoLancamento(Cartoes cartaoLancamento) {
		this.cartaoLancamento = cartaoLancamento;
	}

	public Lojas getLancamentoLoja() {
		return lancamentoLoja;
	}

	public void setLancamentoLoja(Lojas lancamentoLoja) {
		this.lancamentoLoja = lancamentoLoja;
	}

	public Emissores getLancamentoEmissor() {
		return lancamentoEmissor;
	}

	public void setLancamentoEmissor(Emissores lancamentoEmissor) {
		this.lancamentoEmissor = lancamentoEmissor;
	}

}