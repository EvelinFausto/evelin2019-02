package br.com.dbccompany.cartoes.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator( allocationSize = 1, name = "EMISSORES_SEQ", sequenceName = "EMISSORES_SEQ")
public class Emissores extends AbstractEntity{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue( generator = "EMISSORES_SEQ", strategy = GenerationType.SEQUENCE )
	@Column(name = "ID_EMISSOR")
	private Integer id;
	
	@Column( name = "nome", length = 100, nullable = false)
	private String nome;
	
	@Column( name = "taxa", length = 100, nullable = false)
	private String taxa;
	
	@OneToMany(mappedBy = "emissorCartao", cascade = CascadeType.ALL)
	private List<Cartoes> cartoes = new ArrayList<>();
	
	@OneToMany(mappedBy = "lancamentoEmissor", cascade = CascadeType.ALL)
	private List<Lancamentos> lancamentos = new ArrayList<>();

	public String getTaxa() {
		return taxa;
	}

	public void setTaxa(String taxa) {
		this.taxa = taxa;
	}
	@Override
	public Integer getId() {
		return id;
	}
	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
		
		
}
