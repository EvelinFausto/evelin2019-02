package br.com.dbccompany.cartoes.Entity;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.Transaction;

@SuppressWarnings("deprecation")
public class HibernateUtil {
	
	private static final SessionFactory sessionFactory;
	private static final Session session;
	
	static {
		try {
			sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
			session = sessionFactory.openSession();
		} catch (Throwable e) {
			System.err.println("faiou" + e);
			throw new ExceptionInInitializerError(e);
		}
		
	}
	
	public static Session getSession() {
		return session;
	}
	
	public static boolean beginTransaction() {
		Transaction transaction = session.getTransaction();
		if (transaction == null || !transaction.isActive()) {
			transaction = session.beginTransaction();
			return true;
		}
		return false;
	}

}
