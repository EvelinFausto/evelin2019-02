package br.com.dbccompany.cartoes.DTO;

public class LojasDTO {

	private Integer idLoja;
	private String nome;
	
	public Integer getIdLoja() {
		return idLoja;
	}
	public void setIdLoja(Integer idLoja) {
		this.idLoja = idLoja;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
