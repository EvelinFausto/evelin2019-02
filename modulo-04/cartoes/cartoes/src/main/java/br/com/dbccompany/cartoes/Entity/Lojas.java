package br.com.dbccompany.cartoes.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator( allocationSize = 1, name = "LOJAS_SEQ", sequenceName = "LOJAS_SEQ")
public class Lojas extends AbstractEntity{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue( generator = "LOJAS_SEQ", strategy = GenerationType.SEQUENCE )
	@Column(name = "ID_LOJA")
	private Integer id;
	
	@Column( name = "nome", length = 100, nullable = false)
	private String nome;
	
	@OneToMany(mappedBy = "lancamentoLoja", cascade = CascadeType.ALL)
	private List<Lancamentos> lancamentos = new ArrayList<>();

	@ManyToMany( cascade = CascadeType.ALL)
	@JoinTable( name= "loja_x_credenciador", 
					joinColumns = {
						@JoinColumn( name = "id_loja" )}, 
						inverseJoinColumns = { @JoinColumn( name = "id_credenciador")})
	private List<Credenciadores> credenciadores = new ArrayList<>();
	
	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
		
		
}
