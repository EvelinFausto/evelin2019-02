package br.com.dbccompany.cartoes.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator( allocationSize = 1, name = "CARTOES_SEQ", sequenceName = "CARTOES_SEQ")
public class Cartoes extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue( generator = "CARTOES_SEQ", strategy = GenerationType.SEQUENCE )
	@Column(name = "ID_CARTAO")
	private Integer id;
	
	@Column( name = "chip", length = 100, nullable = false)
	private String chip;
	
	@Column( name = "vencimento", length = 100, nullable = false)
	private String vencimento;
	
	@ManyToOne
	@JoinColumn( name = "fk_id_cliente" )
	private Clientes clienteCartao;
	
	@ManyToOne
	@JoinColumn( name = "fk_id_bandeira" )
	private Bandeiras bandeiraCartao;
	
	@ManyToOne
	@JoinColumn( name = "fk_id_emissor" )
	private Emissores emissorCartao;
	
	@OneToMany(mappedBy = "cartaoLancamento", cascade = CascadeType.ALL)
	private List<Lancamentos> lancamentos = new ArrayList<>();

	public String getChip() {
		return chip;
	}

	public void setChip(String chip) {
		this.chip = chip;
	}

	public String getVencimento() {
		return vencimento;
	}

	public void setVencimento(String vencimento) {
		this.vencimento = vencimento;
	}

	public Clientes getClienteCartao() {
		return clienteCartao;
	}

	public void setClienteCartao(Clientes clienteCartao) {
		this.clienteCartao = clienteCartao;
	}

	public Bandeiras getBandeiraCartao() {
		return bandeiraCartao;
	}

	public void setBandeiraCartao(Bandeiras bandeiraCartao) {
		this.bandeiraCartao = bandeiraCartao;
	}

	public Emissores getEmissorCartao() {
		return emissorCartao;
	}

	public void setEmissorCartao(Emissores emissorCartao) {
		this.emissorCartao = emissorCartao;
	}

	public List<Lancamentos> getLancamentos() {
		return lancamentos;
	}

	public void setLancamentos(List<Lancamentos> lancamentos) {
		this.lancamentos = lancamentos;
	}

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	

}