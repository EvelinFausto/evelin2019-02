package br.com.dbccompany.cartoes.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator( allocationSize = 1, name = "CREDENCIADOR_SEQ", sequenceName = "CREDENCIADOR_SEQ")
public class Credenciadores extends AbstractEntity{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue( generator = "CREDENCIADOR_SEQ", strategy = GenerationType.SEQUENCE )
	@Column(name = "ID_CREDENCIADOR")
	private Integer id;
	
	@Column( name = "nome", length = 100, nullable = false)
	private String nome;
	
	@ManyToMany( mappedBy = "credenciadores" )
	private List<Lojas> lojas = new ArrayList<>();
	
	@Override
	public Integer getId() {
		return id;
	}
	
	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
		
		
}