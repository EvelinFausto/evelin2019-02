package br.com.dbcompany.cartoes.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.cartoes.DTO.LojasDTO;
import br.com.dbccompany.cartoes.DAO.LojasDAO;
import br.com.dbccompany.cartoes.Entity.HibernateUtil;
import br.com.dbccompany.cartoes.Entity.Lojas;

public class LojasService {
	
	private static final LojasDAO LOJAS_DAO = new LojasDAO();
	private static final Logger LOG = Logger.getLogger(LojasService.class.getName());
	
	public void salvarLojas(LojasDTO lojasDTO ) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Lojas lojas = LOJAS_DAO.parseFrom(lojasDTO);
		
		try {
			Lojas lojasRes = LOJAS_DAO.buscar(lojas.getId());
			if (lojasRes == null) {
				LOJAS_DAO.criar(lojas);
			}else {
				lojas.setId(lojasRes.getId());
				LOJAS_DAO.atualizar(lojas);
			}
			
			if(started) {
				transaction.commit();
			}
		}catch( Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
}
