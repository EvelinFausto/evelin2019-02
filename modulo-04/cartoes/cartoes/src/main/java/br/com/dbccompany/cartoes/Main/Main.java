package br.com.dbccompany.cartoes.Main;

import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.dbccompany.cartoes.DTO.LojasDTO;
import br.com.dbccompany.cartoes.Entity.HibernateUtil;
import br.com.dbcompany.cartoes.Service.LojasService;

public class Main {

	private static final Logger LOG = Logger.getLogger(Main.class.getName());
	
	public static void main(String[] args) {
		Session session = null;
		Transaction transaction = null;
			session = HibernateUtil.getSession();
			transaction = session.beginTransaction();
			
			LojasService service = new LojasService();
			LojasDTO lojas = new LojasDTO();
			lojas.setNome("MMS");
			service.salvarLojas(lojas);
			
		System.exit(0);
	}
}
