import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) {
        Connection conn = Connector.connect();
        try {
            ResultSet rs = conn.prepareStatement("SELECT TNAME FROM TAB WHERE TNAME = 'PAISES'").executeQuery();
            if (!rs.next()) {
                conn.prepareStatement("CREATE TABLE PAISES(\n"
                        + "ID_PAIS INTEGER NOT NULL PRIMARY KEY,\n"
                        + "NOME VARCHAR(100) NOT NULL\n"
                        + ")\n").execute();
            }
            rs = conn.prepareStatement("SELECT NOME FROM PAISES").executeQuery();
            if(rs.equals("Brasil")) {

                PreparedStatement pst = conn.prepareStatement("INSERT INTO PAISES(ID_PAIS, NOME)"
                        + "values (PAISES_SEQ.NEXTVAL, ?)");
                pst.setString(1, "Horlanda");
                pst.executeUpdate();
            }
            rs = conn.prepareStatement("SELECT * FROM PAISES").executeQuery();
            while (rs.next()){
                System.out.println(String.format("Nome do Pais: %s", rs.getString("nome")));
            }

            rs = conn.prepareStatement("SELECT TNAME FROM TAB WHERE TNAME = 'ESTADOS'").executeQuery();
            if (!rs.next()) {
                conn.prepareStatement("CREATE TABLE ESTADOS (\n" +
                        "    ID_ESTADO INTEGER NOT NULL PRIMARY KEY,\n" +
                        "    NOME VARCHAR2(100) NOT NULL,\n" +
                        "    FK_ID_PAIS INTEGER NOT NULL,\n" +
                        "    FOREIGN KEY (FK_ID_PAIS)\n" +
                        "    REFERENCES PAISES(ID_PAIS)\n" +
                        ")\n").execute();
            }
            PreparedStatement slt = conn.prepareStatement("SELECT NOME FROM ESTADOS where nome = ?");
            slt.setString(0, "Feliz" );
            slt.execute();

            if(rs.next()) {
                PreparedStatement psta = conn.prepareStatement("INSERT INTO ESTADOS(ID_ESTADO, NOME, FK_ID_PAIS)"
                        + "VALUES (ESTADOS_SEQ.NEXTVAL, ?, ?)");
                psta.setString(1, "teste");
                psta.setInt(2, 47);
                psta.executeUpdate();
            }
            rs = conn.prepareStatement("SELECT * FROM ESTADOS").executeQuery();
            while (rs.next()){
                System.out.println(String.format("Nome do Estado: %s", rs.getString("nome")));
            }

            rs = conn.prepareStatement("SELECT TNAME FROM TAB WHERE TNAME = 'CIDADES'").executeQuery();
            if (!rs.next()) {
                conn.prepareStatement("CREATE TABLE CIDADES (\n" +
                        "    ID_CIDADE INTEGER NOT NULL PRIMARY KEY,\n" +
                        "    NOME VARCHAR2(100) NOT NULL,\n" +
                        "    FK_ID_ESTADO INTEGER NOT NULL,\n" +
                        "    FOREIGN KEY (FK_ID_ESTADO)\n" +
                        "    REFERENCES ESTADOS(ID_ESTADO)\n" +
                        ")\n").execute();
            }

            PreparedStatement cidades = conn.prepareStatement("INSERT INTO CIDADES(ID_CIDADE, NOME, FK_ID_ESTADO)"
                    + "VALUES (CIDADES_SEQ.NEXTVAL, ?, ?)");
            cidades.setString(1, "Porto Alegre");
            cidades.setInt(2, 48);
            cidades.executeUpdate();

            rs = conn.prepareStatement("SELECT * FROM CIDADES").executeQuery();
            while (rs.next()){
                System.out.println(String.format("Nome do cidades: %s", rs.getString("nome")));
            }

            rs = conn.prepareStatement("SELECT TNAME FROM TAB WHERE TNAME = 'BAIRROS'").executeQuery();
            if (!rs.next()) {
                conn.prepareStatement("CREATE TABLE BAIRROS (\n" +
                        "    ID_BAIRRO INTEGER NOT NULL PRIMARY KEY,\n" +
                        "    NOME VARCHAR2(100) NOT NULL,\n" +
                        "    FK_ID_CIDADE INTEGER NOT NULL,\n" +
                        "    FOREIGN KEY (FK_ID_CIDADE)\n" +
                        "    REFERENCES CIDADES(ID_CIDADE)\n" +
                        ")\n").execute();
            }
            rs = conn.prepareStatement("select nome from bairros").executeQuery();

            if(rs.equals("feliz")){

            PreparedStatement bairros = conn.prepareStatement("INSERT INTO BAIRROS(ID_BAIRRO, NOME, FK_ID_CIDADE)"
                    + "VALUES (BAIRROS_SEQ.NEXTVAL, ?, ?)");
            bairros.setString(1, "teste4");
            bairros.setInt(2, 44);
            bairros.executeUpdate();
            }

            rs = conn.prepareStatement("SELECT * FROM BAIRROS").executeQuery();
            while (rs.next()) {
                System.out.println(String.format("Nome dos Bairros: %s", rs.getString("nome")));
            }

        }catch (SQLException ex) {
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "ERRO na consulta do Main", ex);
        }
    }
}
