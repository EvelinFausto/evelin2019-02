package br.com.dbccompany.trabalhoFinal.Repository;

import br.com.dbccompany.trabalhoFinal.Entity.Clientes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientesRepository extends CrudRepository<Clientes, Integer> {
}
