package br.com.dbccompany.trabalhoFinal.Repository;

import br.com.dbccompany.trabalhoFinal.Entity.SaldoCliente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoCliente, Integer> {
}
