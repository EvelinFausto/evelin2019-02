package br.com.dbccompany.trabalhoFinal.Entity;

import javax.persistence.*;

@Entity
public class EspacosPacotes extends AbstractEntity {

    @Id
    @Column(name = "idEspacosPacotes")
    @GeneratedValue( generator = "ESPACOS_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(allocationSize = 1, name = "ESPACOS_PACOTES_SEQ", sequenceName = "ESPACOS_PACOTES_SEQ")
    private Integer id;

    @Column(name = "quantidade")
    private Integer quantidade;

    @Column(name = "prazo")
    private Integer prazo;

    @Column(name = "tipoContratacao", nullable = false)
    private TipoContratacao tipoContratacao;

    @ManyToOne
    @JoinColumn( name = "fk_idEspaco" )
    private Espacos espacosPacotes;

    @ManyToOne
    @JoinColumn( name = "fk_idPacote" )
    private Pacotes pacotesEspacos;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Espacos getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(Espacos espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public Pacotes getPacotesEspacos() {
        return pacotesEspacos;
    }

    public void setPacotesEspacos(Pacotes pacotesEspacos) {
        this.pacotesEspacos = pacotesEspacos;
    }
}
