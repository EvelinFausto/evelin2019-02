package br.com.dbccompany.trabalhoFinal.Service;

import br.com.dbccompany.trabalhoFinal.Entity.Clientes;
import br.com.dbccompany.trabalhoFinal.Entity.Pacotes;
import br.com.dbccompany.trabalhoFinal.Repository.ClientesRepository;
import br.com.dbccompany.trabalhoFinal.Repository.PacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public class PacotesService {

    @Autowired
    private PacotesRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Pacotes salvar(Pacotes pacotes) {
        return repository.save(pacotes);
    }

    @Transactional(rollbackFor = Exception.class)
    public Pacotes editar(Integer id, Pacotes pacotes) {
        pacotes.setId(id);
        return repository.save(pacotes);
    }

    public List<Pacotes> todosPacotes() {
        return (List<Pacotes>) repository.findAll();
    }

    public void deletar(Integer id){
        repository.deleteById(id);
    }
}

