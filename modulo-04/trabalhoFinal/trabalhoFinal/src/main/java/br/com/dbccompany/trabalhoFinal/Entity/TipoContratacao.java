package br.com.dbccompany.trabalhoFinal.Entity;

public enum TipoContratacao {

    Minutos, Horas, Turnos, Diarias, Semanas, Meses;
}
