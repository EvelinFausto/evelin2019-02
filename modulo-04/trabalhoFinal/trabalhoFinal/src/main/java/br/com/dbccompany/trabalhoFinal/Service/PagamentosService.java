package br.com.dbccompany.trabalhoFinal.Service;

import br.com.dbccompany.trabalhoFinal.Entity.Pagamentos;
import br.com.dbccompany.trabalhoFinal.Entity.SaldoCliente;
import br.com.dbccompany.trabalhoFinal.Entity.SaldoClienteID;
import br.com.dbccompany.trabalhoFinal.Repository.PagamentosRepository;
import br.com.dbccompany.trabalhoFinal.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentosService {

    @Autowired
    private PagamentosRepository repository;
    private SaldoClienteRepository scR;

    @Transactional(rollbackFor = Exception.class)
    public Pagamentos salvar(Pagamentos pagamentos) {
        if(pagamentos.getContratacaoPagamentos() == null && pagamentos.getClientePacotePagamentos() == null){
            return null;
        }else {
            //LocalDate dataAtual = LocalDate.now();
            //SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
            //Date dataFormatada = formato.parse(dataAtual);
            Date dataAtual = new Date(2019,9,11);
            SaldoCliente saldoCliente = new SaldoCliente();

            Optional<SaldoCliente> result = scR.findById(pagamentos.getClientePacotePagamentos().getId());

            if (result == null) {
                saldoCliente.setId(new SaldoClienteID(pagamentos.getContratacaoPagamentos().getClientesContratacao().getId(), pagamentos.getContratacaoPagamentos().getEspacoContratacao().getId()));
                saldoCliente.setTipoContratacao(pagamentos.getContratacaoPagamentos().getTipoContratacao());
                saldoCliente.setQuantidade(pagamentos.getContratacaoPagamentos().getQuantidade());
                saldoCliente.setVencimento(dataAtual);
                scR.save(saldoCliente);
            }else {
                Date nvdata = (Date) saldoCliente.getVencimento() /*+ pagamentos.getContratacaoPagamentos().getPrazo()*/;
                saldoCliente.setVencimento(nvdata);
                saldoCliente.setId(new SaldoClienteID(pagamentos.getContratacaoPagamentos().getClientesContratacao().getId(), pagamentos.getContratacaoPagamentos().getEspacoContratacao().getId()));
                scR.save(saldoCliente);
            }
            return repository.save(pagamentos);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public Pagamentos editar(Integer id, Pagamentos pagamentos) {
        pagamentos.setId(id);
        return repository.save(pagamentos);
    }

    public List<Pagamentos> todosPagamentos() {
        return (List<Pagamentos>) repository.findAll();
    }

    public void deletar(Integer id){
        repository.deleteById(id);
    }
}

