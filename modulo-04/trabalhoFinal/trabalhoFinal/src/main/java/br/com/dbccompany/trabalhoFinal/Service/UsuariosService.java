package br.com.dbccompany.trabalhoFinal.Service;

import br.com.dbccompany.trabalhoFinal.Entity.Usuarios;
import br.com.dbccompany.trabalhoFinal.Repository.UsuariosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UsuariosService {

    @Autowired
    private UsuariosRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Usuarios salvar(Usuarios usuarios) {
        if(usuarios.getSenha().length() < 6){
            return null;
        }else {
            return repository.save(usuarios);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public Usuarios editar(Integer id, Usuarios usuarios) {
        usuarios.setId(id);
        return repository.save(usuarios);
    }

    public List<Usuarios> todosUsuarios() {
        return (List<Usuarios>) repository.findAll();
    }

    public void deletar(Integer id){
        repository.deleteById(id);
    }
}

