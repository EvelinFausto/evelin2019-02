package br.com.dbccompany.trabalhoFinal.Repository;


    import br.com.dbccompany.trabalhoFinal.Entity.Acessos;
    import org.springframework.data.repository.CrudRepository;
    import org.springframework.stereotype.Repository;

@Repository
public interface AcessosRepository extends CrudRepository<Acessos, Integer> {

}
