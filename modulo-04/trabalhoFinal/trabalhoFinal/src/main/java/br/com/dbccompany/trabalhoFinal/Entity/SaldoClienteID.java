package br.com.dbccompany.trabalhoFinal.Entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SaldoClienteID implements Serializable {

    @Column( name = "ID_CLIENTE" )
    private Integer idCliente;

    @Column( name = "ID_ESPACO" )
    private Integer idEspaco;

    public SaldoClienteID( Integer idCliente, Integer idEspaco) {
        this.idCliente = idCliente;
        this.idEspaco = idEspaco;
    }
}
