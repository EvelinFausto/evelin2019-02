package br.com.dbccompany.trabalhoFinal.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Contratacao extends AbstractEntity {

    @Id
    @Column(name = "idContratacao")
    @GeneratedValue( generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(allocationSize = 1, name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
    private Integer id;

    @Column(name = "quantidade", nullable = false)
    private Integer quantidade;

    @Column(name = "desconto", nullable = true)
    private Double desconto;

    @Column(name = "prazo", nullable = false)
    private Integer prazo;

    @Column(name = "tipoContratacao")
    private TipoContratacao tipoContratacao;

    @ManyToOne
    @JoinColumn( name = "fk_idCliente")
    private Clientes clientesContratacao;

    @ManyToOne
    @JoinColumn( name = "fk_idEspaco" )
    private Espacos espacosContratacao;

    @OneToMany(mappedBy = "contratacaoPagamentos", cascade = CascadeType.ALL)
    private List<Pagamentos> pagamentos = new ArrayList<>();

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Clientes getClientesContratacao() {
        return clientesContratacao;
    }

    public void setClientesContratacao(Clientes clientesContratacao) {
        clientesContratacao = clientesContratacao;
    }

    public Espacos getEspacoContratacao() {
        return espacosContratacao;
    }

    public void setEspacoContratacao(Espacos espacoContratacao) {
        espacosContratacao = espacoContratacao;
    }
}
