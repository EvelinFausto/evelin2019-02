package br.com.dbccompany.trabalhoFinal.Repository;

import br.com.dbccompany.trabalhoFinal.Entity.Espacos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EspacosRepository extends CrudRepository<Espacos, Integer> {
}
