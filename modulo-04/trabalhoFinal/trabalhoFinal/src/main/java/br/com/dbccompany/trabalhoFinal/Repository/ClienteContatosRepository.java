package br.com.dbccompany.trabalhoFinal.Repository;

import br.com.dbccompany.trabalhoFinal.Entity.ClienteContato;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteContatosRepository extends CrudRepository<ClienteContato, Integer> {
}
