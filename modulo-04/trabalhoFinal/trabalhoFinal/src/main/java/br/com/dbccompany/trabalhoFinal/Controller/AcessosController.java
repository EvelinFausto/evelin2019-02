package br.com.dbccompany.trabalhoFinal.Controller;

import br.com.dbccompany.trabalhoFinal.Entity.Acessos;
import br.com.dbccompany.trabalhoFinal.Service.AcessosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/acessos")
public class AcessosController {

    @Autowired
    AcessosService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Acessos> todosAcessos() {
        return service.todosAcessos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public String novo(@RequestBody Acessos acessos) {
        return service.salvar(acessos);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Acessos editar( @PathVariable Integer id, @RequestBody Acessos acessos) {
        return service.editar(id, acessos);
    }

    @GetMapping( value = "/deletar/{id}" )
    @ResponseBody
    public List<Acessos> deletar(@PathVariable Integer id){
        service.deletar(id);
        return service.todosAcessos();
    }
}

