package br.com.dbccompany.trabalhoFinal.Controller;

import br.com.dbccompany.trabalhoFinal.Entity.Pacotes;
import br.com.dbccompany.trabalhoFinal.Service.PacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pacotes")
public class PacotesController {

    @Autowired
    PacotesService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Pacotes> todosPacotes() {
        return service.todosPacotes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Pacotes novo(@RequestBody Pacotes pacotes) {
        return service.salvar(pacotes);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Pacotes editar( @PathVariable Integer id, @RequestBody Pacotes pacotes) {
        return service.editar(id, pacotes);
    }

    @GetMapping( value = "/deletar/{id}" )
    @ResponseBody
    public List<Pacotes> deletar(@PathVariable Integer id){
        service.deletar(id);
        return service.todosPacotes();
    }
}

