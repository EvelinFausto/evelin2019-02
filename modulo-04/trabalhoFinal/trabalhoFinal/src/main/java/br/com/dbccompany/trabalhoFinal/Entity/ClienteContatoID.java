package br.com.dbccompany.trabalhoFinal.Entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ClienteContatoID implements Serializable {

    @Column( name = "ID_CLIENTE" )
    private Integer idCliente;

    @Column( name = "ID_TIPOCONTATO" )
    private Integer idTipoContato;

    public ClienteContatoID( Integer idCliente, Integer idTipoContato) {
        this.idCliente = idCliente;
        this.idTipoContato = idTipoContato;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public Integer getIdTipoContato() {
        return idTipoContato;
    }
}
