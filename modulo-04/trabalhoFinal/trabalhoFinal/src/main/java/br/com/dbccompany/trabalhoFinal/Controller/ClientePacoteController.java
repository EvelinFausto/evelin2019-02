package br.com.dbccompany.trabalhoFinal.Controller;

import br.com.dbccompany.trabalhoFinal.Entity.ClientesPacotes;
import br.com.dbccompany.trabalhoFinal.Service.ClientePacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clientesPacotes")
public class ClientePacoteController {

    @Autowired
    ClientePacoteService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<ClientesPacotes> todosClientesPacotes() {
        return service.todosClientesPacotes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public ClientesPacotes novo(@RequestBody ClientesPacotes clientesPacotes) {
        return service.salvar(clientesPacotes);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ClientesPacotes editar( @PathVariable Integer id, @RequestBody ClientesPacotes clientesPacotes) {
        return service.editar(id, clientesPacotes);
    }

    @GetMapping( value = "/deletar/{id}" )
    @ResponseBody
    public List<ClientesPacotes> deletar(@PathVariable Integer id){
        service.deletar(id);
        return service.todosClientesPacotes();
    }
}

