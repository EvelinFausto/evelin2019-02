package br.com.dbccompany.trabalhoFinal.Controller;

import br.com.dbccompany.trabalhoFinal.Entity.Espacos;
import br.com.dbccompany.trabalhoFinal.Service.EspacosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Controller
@RequestMapping("/api/espacos")
public class EspacosController {

    @Autowired
    EspacosService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Espacos> todosEspacos() {
        return service.todosEspacos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Espacos novo(@RequestBody Espacos espacos) {
        return service.salvar(espacos);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Espacos editar( @PathVariable Integer id, @RequestBody Espacos espacos) {
        return service.editar(id, espacos);
    }

    @GetMapping( value = "/deletar/{id}" )
    @ResponseBody
    public List<Espacos> deletar(@PathVariable Integer id){
        service.deletar(id);
        return service.todosEspacos();
    }
}

