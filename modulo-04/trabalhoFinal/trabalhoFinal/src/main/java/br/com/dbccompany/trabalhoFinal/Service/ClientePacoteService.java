package br.com.dbccompany.trabalhoFinal.Service;

import br.com.dbccompany.trabalhoFinal.Entity.Clientes;
import br.com.dbccompany.trabalhoFinal.Entity.ClientesPacotes;
import br.com.dbccompany.trabalhoFinal.Repository.ClientesPacotesRepository;
import br.com.dbccompany.trabalhoFinal.Repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClientePacoteService {

    @Autowired
    private ClientesPacotesRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public ClientesPacotes salvar(ClientesPacotes clientesPacotes) {
        return repository.save(clientesPacotes);
    }

    @Transactional(rollbackFor = Exception.class)
    public ClientesPacotes editar(Integer id, ClientesPacotes clientesPacotes) {
        clientesPacotes.setId(id);
        return repository.save(clientesPacotes);
    }

    public List<ClientesPacotes> todosClientesPacotes() {
        return (List<ClientesPacotes>) repository.findAll();
    }

    public void deletar(Integer id){
        repository.deleteById(id);
    }
}

