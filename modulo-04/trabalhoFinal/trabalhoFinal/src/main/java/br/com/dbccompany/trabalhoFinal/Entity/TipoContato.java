package br.com.dbccompany.trabalhoFinal.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class TipoContato extends AbstractEntity{

    @Id
    @Column(name = "idTipoContato")
    @GeneratedValue( generator = "TIPOCONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(allocationSize = 1, name = "TIPOCONTATO_SEQ", sequenceName = "TIPOCONTATO_SEQ")
    private Integer id;

    @Column(name = "nome", length = 100, nullable = true)
    private String nome;

    @OneToMany(mappedBy = "tipoContato", cascade = CascadeType.ALL)
    private List<Contatos> contatos = new ArrayList<>();

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
