package br.com.dbccompany.trabalhoFinal.Service;

import br.com.dbccompany.trabalhoFinal.Entity.Espacos;
import br.com.dbccompany.trabalhoFinal.Repository.EspacosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public class EspacosService {

    @Autowired
    private EspacosRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Espacos salvar(Espacos espacos) {
        return repository.save(espacos);
    }

    @Transactional(rollbackFor = Exception.class)
    public Espacos editar(Integer id, Espacos espacos) {
        espacos.setId(id);
        return repository.save(espacos);
    }

    public List<Espacos> todosEspacos() {
        return (List<Espacos>) repository.findAll();
    }

    public void deletar(Integer id){
        repository.deleteById(id);
    }
}

