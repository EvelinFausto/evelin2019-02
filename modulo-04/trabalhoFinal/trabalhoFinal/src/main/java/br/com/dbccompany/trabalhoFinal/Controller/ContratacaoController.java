package br.com.dbccompany.trabalhoFinal.Controller;

import br.com.dbccompany.trabalhoFinal.Entity.Contratacao;
import br.com.dbccompany.trabalhoFinal.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Controller
@RequestMapping("/api/contratacoes")
public class ContratacaoController {

    @Autowired
    ContratacaoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Contratacao> todosContratacao() {
        return service.todosContratacao();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Integer novo(@RequestBody Contratacao contratacao) {
        return service.salvar(contratacao);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Contratacao editar( @PathVariable Integer id, @RequestBody Contratacao contratacao) {
        return service.editar(id, contratacao);
    }

    @GetMapping( value = "/deletar/{id}" )
    @ResponseBody
    public List<Contratacao> deletar(@PathVariable Integer id){
        service.deletar(id);
        return service.todosContratacao();
    }
}

