package br.com.dbccompany.trabalhoFinal.Service;

import br.com.dbccompany.trabalhoFinal.Entity.ClienteContato;
import br.com.dbccompany.trabalhoFinal.Entity.Clientes;
import br.com.dbccompany.trabalhoFinal.Repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClientesService {

    @Autowired
    private ClientesRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Clientes salvar(Clientes cliente) {
        ClienteContato clienteContato = new ClienteContato();


        return repository.save(cliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public Clientes editar(Integer id, Clientes cliente) {
        cliente.setId(id);
        return repository.save(cliente);
    }

    public List<Clientes> todosClientes() {
        return (List<Clientes>) repository.findAll();
    }

    public void deletar(Integer id){
        repository.deleteById(id);
    }
}

