package br.com.dbccompany.trabalhoFinal.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Clientes extends  AbstractEntity{

    @Id
    @Column(name = "idClientes")
    @GeneratedValue( generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(allocationSize = 1, name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ")
    private Integer id;

    @Column(name = "nome", length = 100, nullable = false)
    private String nome;

    @Column(name = "cpf", length = 14, nullable = false, unique = true)
    private String cpf;

    @Column(name = "dataNascimento", length = 14, nullable = false)
    private String dataNascimento;

    @OneToMany(mappedBy = "ClientesPacotes", cascade = CascadeType.ALL)
    private List<ClientesPacotes> clientesPacotes = new ArrayList<>();

    @OneToMany(mappedBy = "clientesContratacao", cascade = CascadeType.ALL)
    private List<Contratacao> contratacao = new ArrayList<>();

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ClientesPacotes> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotes> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public List<Contratacao> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<Contratacao> contratacao) {
        this.contratacao = contratacao;

    }
}
