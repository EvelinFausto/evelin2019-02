package br.com.dbccompany.trabalhoFinal.Repository;

import br.com.dbccompany.trabalhoFinal.Entity.Pagamentos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PagamentosRepository extends CrudRepository<Pagamentos, Integer> {
}
