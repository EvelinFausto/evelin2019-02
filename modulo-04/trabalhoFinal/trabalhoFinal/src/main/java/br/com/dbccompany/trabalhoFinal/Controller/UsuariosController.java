package br.com.dbccompany.trabalhoFinal.Controller;

import br.com.dbccompany.trabalhoFinal.Entity.Usuarios;
import br.com.dbccompany.trabalhoFinal.Service.UsuariosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/usuarios")
public class UsuariosController {

    @Autowired
    UsuariosService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Usuarios> todosUsuarios() {
        return service.todosUsuarios();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Usuarios novo(@RequestBody Usuarios usuario) {
        return service.salvar(usuario);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Usuarios editar( @PathVariable Integer id, @RequestBody Usuarios usuario) {
        return service.editar(id, usuario);
    }

    @GetMapping( value = "/deletar/{id}" )
    @ResponseBody
    public List<Usuarios> deletar(@PathVariable Integer id){
        service.deletar(id);
        return service.todosUsuarios();
    }
}

