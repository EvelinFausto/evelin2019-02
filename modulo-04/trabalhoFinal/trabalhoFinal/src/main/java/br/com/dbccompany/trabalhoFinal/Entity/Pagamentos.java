package br.com.dbccompany.trabalhoFinal.Entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Pagamentos extends  AbstractEntity{

    @Id
    @Column(name = "idPagamentos")
    @GeneratedValue( generator = "PAGAMENTOS_SEQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(allocationSize = 1, name = "PAGAMENTOS_SEQ", sequenceName = "PAGAMENTOS_SEQ")
    private Integer id;

    @Column(name = "tipoPagmento")
    private TipoPagamento tipoPagamento;

    @ManyToOne
    @JoinColumn( name = "fk_idClientePacote" )
    private ClientesPacotes clientePacotePagamento;

    @ManyToOne
    @JoinColumn( name = "fk_idContratacao" )
    private Contratacao contratacaoPagamentos;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public ClientesPacotes getClientePacotePagamentos() {
        return clientePacotePagamento;
    }

    public void setClientePacotePagamentos(ClientesPacotes clientePacotePagamento) {
        this.clientePacotePagamento = clientePacotePagamento;
    }

    public Contratacao getContratacaoPagamentos() {
        return contratacaoPagamentos;
    }

    public void setContratacaoPagamentos(Contratacao contratacaoPagamentos) {
        this.contratacaoPagamentos = contratacaoPagamentos;
    }
}