package br.com.dbccompany.trabalhoFinal.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EmbeddedId;


@Entity
public class SaldoCliente {

    @EmbeddedId
    private SaldoClienteID id;

    @Column(name = "tipoContratacao", nullable = false)
    private TipoContratacao tipoContratacao;

    @Column(name = "quantidade", nullable = false)
    private Integer quantidade;

    @Column(name = "vencimento", nullable = false)
    private Date vencimento;

    @OneToMany(mappedBy = "clienteSaldo", cascade = CascadeType.ALL)
    private List<Acessos> acessos = new ArrayList<>();


    public SaldoClienteID getId() {
        return id;
    }

    public void setId(SaldoClienteID id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public List<Acessos> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<Acessos> acessos) {
        this.acessos = acessos;
    }

}

