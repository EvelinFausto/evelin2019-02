package br.com.dbccompany.trabalhoFinal.Controller;

import br.com.dbccompany.trabalhoFinal.Entity.Clientes;
import br.com.dbccompany.trabalhoFinal.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clientes")
public class ClientesController {

    @Autowired
    ClientesService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Clientes> todosClientes() {
        return service.todosClientes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Clientes novo(@RequestBody Clientes cliente) {
        return service.salvar(cliente);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Clientes editar( @PathVariable Integer id, @RequestBody Clientes cliente) {
        return service.editar(id, cliente);
    }

    @GetMapping( value = "/deletar/{id}" )
    @ResponseBody
    public List<Clientes> deletar(@PathVariable Integer id){
        service.deletar(id);
        return service.todosClientes();
    }
}


