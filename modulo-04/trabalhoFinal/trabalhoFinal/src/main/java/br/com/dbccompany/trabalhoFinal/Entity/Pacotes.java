package br.com.dbccompany.trabalhoFinal.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Pacotes extends AbstractEntity {

    @Id
    @Column(name = "idPacote")
    @GeneratedValue( generator = "PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(allocationSize = 1, name = "PACOTES_SEQ", sequenceName = "PACOTES_SEQ")
    private Integer id;

    @Column(name = "valor")
    private Integer valor;

    @OneToMany(mappedBy = "pacotesEspacos", cascade = CascadeType.ALL)
    private List<EspacosPacotes> pacotesEspacos = new ArrayList<>();

    @OneToMany(mappedBy = "PacotesClientes", cascade = CascadeType.ALL)
    private List<ClientesPacotes> pacotesClientes = new ArrayList<>();

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }
}
