package br.com.dbccompany.trabalhoFinal.Entity;

public enum TipoPagamento {

    Debito, Credito, Dinheiro, Transferencia

}
