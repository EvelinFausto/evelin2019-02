package br.com.dbccompany.trabalhoFinal.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Espacos extends AbstractEntity {

    @Id
    @Column(name = "idEspaco")
    @GeneratedValue( generator = "ESPACOS_SEQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(allocationSize = 1, name = "ESPACOS_SEQ", sequenceName = "ESPACOS_SEQ")
    private Integer id;

    @Column(name = "nome", length = 100, nullable = false, unique = true)
    private String nome;

    @Column(name = "qtdPessoas", length = 100, nullable = false)
    private Integer qtdPessoas;

    @Column(name = "valor", length = 100, nullable = false)
    private Integer valor;

    @OneToMany(mappedBy = "espacosPacotes", cascade = CascadeType.ALL)
    private List<EspacosPacotes> espacosPacotes = new ArrayList<>();

    @OneToMany(mappedBy = "espacosContratacao", cascade = CascadeType.ALL)
    private List<Contratacao> contratacao = new ArrayList<>();

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public List<Contratacao> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<Contratacao> contratacao) {
        this.contratacao = contratacao;
    }
}
