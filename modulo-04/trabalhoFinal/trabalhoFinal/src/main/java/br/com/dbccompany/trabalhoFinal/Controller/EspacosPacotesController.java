package br.com.dbccompany.trabalhoFinal.Controller;

import br.com.dbccompany.trabalhoFinal.Entity.EspacosPacotes;
import br.com.dbccompany.trabalhoFinal.Service.EspacoPacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espacosPacotes")
public class EspacosPacotesController {

    @Autowired
    EspacoPacoteService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<EspacosPacotes> todosEspacosPacotes() {
        return service.todosEspacosPacotes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public EspacosPacotes novo(@RequestBody EspacosPacotes espacosPacotes) {
        return service.salvar(espacosPacotes);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public EspacosPacotes editar( @PathVariable Integer id, @RequestBody EspacosPacotes espacosPacotes) {
        return service.editar(id, espacosPacotes);
    }

    @GetMapping( value = "/deletar/{id}" )
    @ResponseBody
    public List<EspacosPacotes> deletar(@PathVariable Integer id){
        service.deletar(id);
        return service.todosEspacosPacotes();
    }
}

