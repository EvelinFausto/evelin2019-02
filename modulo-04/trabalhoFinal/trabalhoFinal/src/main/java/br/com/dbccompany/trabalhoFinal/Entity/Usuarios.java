package br.com.dbccompany.trabalhoFinal.Entity;

import javax.persistence.*;

@Entity
public class Usuarios extends AbstractEntity{

    @Id
    @Column(name = "idUsuario")
    @GeneratedValue( generator = "USUARIOS_SEQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(allocationSize = 1, name = "USUARIOS_SEQ", sequenceName = "USUARIOS_SEQ")
    private Integer id;

    @Column(name = "nome", length = 100)
    private String nome;

    @Column(name = "email", length = 100, nullable = true, unique = true)
    private String email;

    @Column(name = "login", length = 100, unique = true)
    private String login;

    @Column(name = "senha")
    private String senha;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha){
        this.senha = senha;
    }
}
