package br.com.dbccompany.trabalhoFinal.Entity;


import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class ClienteContato {

    @EmbeddedId
    private ClienteContatoID id;

    @Column(name = "valor", nullable = false)
    private String valor;

    public ClienteContatoID getId() {
        return id;
    }

    public void setId(ClienteContatoID id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}

