package br.com.dbccompany.trabalhoFinal.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class ClientesPacotes extends AbstractEntity {

    @Column(name = "idClientesPacotes")
    @GeneratedValue( generator = "CLIENTES_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(allocationSize = 1, name = "CLIENTES_PACOTES_SEQ", sequenceName = "CLIENTES_PACOTES_SEQ")
    @Id
    private Integer id;

    @Column(name = "quantidade")
    private Integer quantidade;

    @ManyToOne
    @JoinColumn( name = "fk_idPacote" )
    private Pacotes PacotesClientes;

    @ManyToOne
    @JoinColumn( name = "fk_idCliente" )
    private Clientes ClientesPacotes;

    @OneToMany(mappedBy = "clientePacotePagamento", cascade = CascadeType.ALL)
    private List<Pagamentos> pagamentos = new ArrayList<>();

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Pacotes getPacotesClientes() {
        return PacotesClientes;
    }

    public void setPacotesClientes(Pacotes pacotesClientes) {
        PacotesClientes = pacotesClientes;
    }

    public Clientes getClientesPacotes() {
        return ClientesPacotes;
    }

    public void setClientesPacotes(Clientes clientesPacotes) {
        ClientesPacotes = clientesPacotes;
    }

    public List<Pagamentos> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<Pagamentos> pagamentos) {
        this.pagamentos = pagamentos;
    }
}
