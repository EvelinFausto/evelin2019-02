package br.com.dbccompany.trabalhoFinal.Controller;

import br.com.dbccompany.trabalhoFinal.Entity.Pagamentos;
import br.com.dbccompany.trabalhoFinal.Service.PagamentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pagamentos")
public class PagamentosController {

    @Autowired
    PagamentosService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Pagamentos> todosPagamentos() {
        return service.todosPagamentos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Pagamentos novo(@RequestBody Pagamentos pagamentos) {
        return service.salvar(pagamentos);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Pagamentos editar( @PathVariable Integer id, @RequestBody Pagamentos pagamentos) {
        return service.editar(id, pagamentos);
    }

    @GetMapping( value = "/deletar/{id}" )
    @ResponseBody
    public List<Pagamentos> deletar(@PathVariable Integer id){
        service.deletar(id);
        return service.todosPagamentos();
    }


}

