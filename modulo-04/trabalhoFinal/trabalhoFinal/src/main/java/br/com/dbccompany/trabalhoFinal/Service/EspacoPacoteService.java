package br.com.dbccompany.trabalhoFinal.Service;

import br.com.dbccompany.trabalhoFinal.Entity.Clientes;
import br.com.dbccompany.trabalhoFinal.Entity.EspacosPacotes;
import br.com.dbccompany.trabalhoFinal.Repository.ClientesRepository;
import br.com.dbccompany.trabalhoFinal.Repository.EspacosPacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EspacoPacoteService {

    @Autowired
    private EspacosPacotesRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public EspacosPacotes salvar(EspacosPacotes espacosPacotes) {
        return repository.save(espacosPacotes);
    }

    @Transactional(rollbackFor = Exception.class)
    public EspacosPacotes editar(Integer id, EspacosPacotes espacosPacotes) {
        espacosPacotes.setId(id);
        return repository.save(espacosPacotes);
    }

    public List<EspacosPacotes> todosEspacosPacotes() {
        return (List<EspacosPacotes>) repository.findAll();
    }

    public void deletar(Integer id){
        repository.deleteById(id);
    }
}

