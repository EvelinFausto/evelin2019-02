package br.com.dbccompany.trabalhoFinal.Controller;

import br.com.dbccompany.trabalhoFinal.Entity.ClienteContato;
import br.com.dbccompany.trabalhoFinal.Service.ClienteContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clienteContatos")
public class ClienteContatosController {

    @Autowired
    ClienteContatoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<ClienteContato> todosContatos() {
        return service.todosContatos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public ClienteContato novo(@RequestBody ClienteContato clienteContato) {
        return service.salvar(clienteContato);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ClienteContato editar( @PathVariable Integer id, @RequestBody ClienteContato clienteContato) {
        return service.editar(id, clienteContato);
    }

    @GetMapping( value = "/deletar/{id}" )
    @ResponseBody
    public List<ClienteContato> deletar(@PathVariable Integer id){
        service.deletar(id);
        return service.todosContatos();
    }
}

