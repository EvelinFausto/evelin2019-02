package br.com.dbccompany.trabalhoFinal.Service;

import br.com.dbccompany.trabalhoFinal.Entity.Acessos;
import br.com.dbccompany.trabalhoFinal.Entity.SaldoCliente;
import br.com.dbccompany.trabalhoFinal.Repository.AcessosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;

@Service
public class AcessosService {

    @Autowired
    private AcessosRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public String salvar(Acessos acessos) {
        Date dataAtual = new Date(2019,9,11);
        if (acessos.getData() == null){
            acessos.setData( dataAtual );
        }
        repository.save(acessos);
        return verificacaoDeSaldo(acessos);
    }

    public String verificacaoDeSaldo(Acessos acessos){
        Date dataAtual = new Date(2019,9,11);
        if (acessos.getClienteSaldo().getVencimento() == null /*dataAtual*/){
            SaldoCliente sd = new SaldoCliente();
            sd.setQuantidade(0);
            return "Saldo Insuficiente";
        }else {
            return "saldo de ´${acessos.getClienteSaldo().getQuantidade()}´";
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public Acessos editar(Integer id, Acessos acessos) {
        acessos.setId(id);
        return repository.save(acessos);
    }

    public List<Acessos> todosAcessos() {
        return (List<Acessos>) repository.findAll();
    }

    public void deletar(Integer id){
        repository.deleteById(id);
    }
}

