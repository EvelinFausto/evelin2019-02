package br.com.dbccompany.trabalhoFinal.Service;

import br.com.dbccompany.trabalhoFinal.Entity.Contratacao;
import br.com.dbccompany.trabalhoFinal.Repository.ContratacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public class ContratacaoService {

    @Autowired
    private ContratacaoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Integer salvar(Contratacao contratacao) {
        repository.save(contratacao);
        Contratacao contratacao1 = new Contratacao();
        Integer Total = (contratacao.getQuantidade() * contratacao1.getEspacoContratacao().getValor());
        return Total;
    }

    @Transactional(rollbackFor = Exception.class)
    public Contratacao editar(Integer id, Contratacao contratacao) {
        contratacao.setId(id);
        return repository.save(contratacao);
    }

    public List<Contratacao> todosContratacao() {
        return (List<Contratacao>) repository.findAll();
    }

    public void deletar(Integer id){
        repository.deleteById(id);
    }
}

