package br.com.dbccompany.trabalhoFinal.Service;

import br.com.dbccompany.trabalhoFinal.Entity.ClienteContato;
import br.com.dbccompany.trabalhoFinal.Repository.ClienteContatosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClienteContatoService {

    @Autowired
    private ClienteContatosRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public ClienteContato salvar(ClienteContato clienteContato) {
        return repository.save(clienteContato);
    }

    @Transactional(rollbackFor = Exception.class)
    public ClienteContato editar(Integer id, ClienteContato clienteContato) {
        return repository.save(clienteContato);
    }

    public List<ClienteContato> todosContatos() {
        return (List<ClienteContato>) repository.findAll();
    }

    public void deletar(Integer id){
        repository.deleteById(id);
    }
}
