package br.com.dbccompany.trabalhoFinal.Entity;

        import javax.persistence.*;
        import java.sql.Date;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Acessos extends  AbstractEntity{

    @Id
    @Column(name = "idAcessos")
    @GeneratedValue( generator = "ACESSOS_SEQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(allocationSize = 1, name = "ACESSOS_SEQ", sequenceName = "ACESSOS_SEQ")
    private Integer id;

    @Column(name = "entrada")
    private Boolean entrada;

    @Column(name = "data")
    private Date data;

    @Column(name = "excecao")
    private Integer excecao;

    @ManyToOne
    @JoinColumns( {
            @JoinColumn( name = "fkIdClienteSaldo"),
            @JoinColumn( name = "fkIdEspacoSaldo")
    })
    private SaldoCliente clienteSaldo;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getEntrada() {
        return entrada;
    }

    public void setEntrada(Boolean entrada) {
        this.entrada = entrada;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Integer getExcecao() {
        return excecao;
    }

    public void setExcecao(Integer excecao) {
        this.excecao = excecao;
    }

    public SaldoCliente getClienteSaldo() {
        return clienteSaldo;
    }

    public void setClienteSaldo(SaldoCliente clienteSaldo) {
        this.clienteSaldo = clienteSaldo;
    }
}





