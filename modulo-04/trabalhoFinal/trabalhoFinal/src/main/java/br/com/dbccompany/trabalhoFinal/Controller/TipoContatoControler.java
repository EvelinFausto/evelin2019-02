package br.com.dbccompany.trabalhoFinal.Controller;

import br.com.dbccompany.trabalhoFinal.Entity.TipoContato;
import br.com.dbccompany.trabalhoFinal.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/tipoContato")

public class TipoContatoControler {

    @Autowired
    TipoContatoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<TipoContato> todosTipoContato() {
        return service.todosTipoContato();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public TipoContato novo(@RequestBody TipoContato tipoContato) {
        return service.salvar(tipoContato);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public TipoContato editar( @PathVariable Integer id, @RequestBody TipoContato tipoContato) {
        return service.editar(id, tipoContato);
    }

    @GetMapping( value = "/deletar/{id}" )
    @ResponseBody
    public List<TipoContato> deletar(@PathVariable Integer id){
        service.deletar(id);
        return service.todosTipoContato();
    }
}
