package br.com.dbccompany.trabalhoFinal.Usuarios;

import br.com.dbccompany.trabalhoFinal.Entity.Usuarios;
import br.com.dbccompany.trabalhoFinal.Repository.UsuariosRepository;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UsuariosTest extends TestCase {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UsuariosRepository usuariosRepository;

    @Test
    public void procurarUsuarioPorNome(){
        Usuarios usuarios = new Usuarios();
        usuarios.setNome("Joao");
        entityManager.persist(usuarios);
        entityManager.flush();
        Usuarios resposta = usuariosRepository.findByNome(usuarios.getNome());
        assertThat("erro").isEqualTo(usuarios.getNome());
    }
}