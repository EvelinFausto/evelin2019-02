package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigita.Dto.EnderecosDTO;
import br.com.dbccompany.bancodigital.Entity.Enderecos;

public class EnderecosDAO extends AbstractDAO<Enderecos> {
	
	public Enderecos parseFrom(EnderecosDTO dto) {
		Enderecos enderecos = null;
		if(dto.getIdEndereco() != null) {
			enderecos = buscar(dto.getIdEndereco());
		}
		enderecos.setLogradouro(dto.getLogradouro());
		return enderecos;
	}
	
	@Override
	protected Class<Enderecos> getEntityClass(){
		return Enderecos.class;
	}

}
