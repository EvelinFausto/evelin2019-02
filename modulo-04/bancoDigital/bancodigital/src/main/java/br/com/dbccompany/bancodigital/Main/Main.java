package br.com.dbccompany.bancodigital.Main;

import java.util.logging.Logger;

import br.com.dbccompany.bancodigita.Dto.EnderecosDTO;
import br.com.dbccompany.bancodigita.Dto.PaisesDTO;
import br.com.dbcompany.bancodigital.Service.EnderecosService;
import br.com.dbcompany.bancodigital.Service.PaisesService;

public class Main {
	
	private static final Logger LOG = Logger.getLogger(Main.class.getName());
	
	public static void main(String[] args) {
		PaisesService service = new PaisesService();
		PaisesDTO paises = new PaisesDTO();
		paises.setNome("Argentina");
		service.salvarPaises(paises);
		
		EnderecosService ser = new EnderecosService();
		EnderecosDTO enderecos = new EnderecosDTO();
		enderecos.setLogradouro("logradouro");
		ser.salvarEnderecos(enderecos);
		
		
		/*Paises paises = new Paises();
		paises.setNome("Argentina");
		service.salvarPaises(paises);*/
		System.exit(0);
	}

		/*public static void main(String[] args) {
			Session session = null;
			Transaction transaction = null;
			try {
				session = HibernateUtil.getSession();
				transaction = session.beginTransaction();
				
				Paises paises = new Paises();
				paises.setNome("Brasil");
				
				session.save(paises);
				
				//session.createQuery("select * from paises;").executeUpdate();
				
				Criteria criteria = session.createCriteria(Paises.class);
				criteria.createAlias("nome", "nome_pais");
				criteria.add(
						Restrictions.isNotNull("nome")
						);
				List<Paises> lstPaises = criteria.list();
				
				transaction.commit();
			}catch (Exception e) {
				if (transaction != null) {
					transaction.rollback();
				}
				LOG.log(Level.SEVERE, e.getMessage(), e);
				System.exit(1);
			}finally {
				if(session != null) {
					session.close();
				}
			}
			System.exit(0);
		}*/
}
