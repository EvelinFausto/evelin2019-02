package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator( allocationSize = 1, name = "BANCOS_SEQ", sequenceName = "BANCOS_SEQ" )
public class Bancos extends AbstractEntity{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID_BANCO")
	@GeneratedValue(generator = "BANCOS_SEQ", strategy = GenerationType.SEQUENCE )
	private Integer id;
	private String codigo;
	private Integer nome;
	
	@OneToMany(mappedBy = "banco", cascade = CascadeType.ALL)
	private List<Agencias> agencias = new ArrayList<>();

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Integer getNome() {
		return nome;
	}

	public void setNome(Integer nome) {
		this.nome = nome;
	}

	public List<Agencias> getAgencias() {
		return agencias;
	}

	public void setAgencias(List<Agencias> agencias) {
		this.agencias = agencias;
	}

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;		
	}
	
}

