package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Correntistas extends AbstractEntity{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "CORRENTISTAS_SEQ", strategy = GenerationType.SEQUENCE )
	@Column(name = "ID_CORRENTISTA")
	@SequenceGenerator( allocationSize = 1, name = "CORRENTISTAS_SEQ", sequenceName = "CORRENTISTAS_SEQ" )
	private Integer id;
	
	@Column( name = "razaoSocial", length = 100, nullable = true)
	private String razaoSocial;
	
	@Column( name = "cnpj", length = 100, nullable = true)
	private String cnpj;
	
	@Column( name = "tipo", length = 100, nullable = false)
	private TiposContas tipo;
	
	@ManyToMany( cascade = CascadeType.ALL)
	@JoinTable( name= "correntista_x_clientes", 
					joinColumns = {
						@JoinColumn( name = "id_correntista" )}, 
						inverseJoinColumns = { @JoinColumn( name = "id_cliente")})
	private List<Clientes> clientes = new ArrayList<>();
	
	@ManyToMany( mappedBy = "correntistas" )
	private List<Agencias> agencias = new ArrayList<>();
	
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public TiposContas getTipo() {
		return tipo;
	}
	public void setTipo(TiposContas tipo) {
		this.tipo = tipo;
	}
	@Override
	public Integer getId() {
		return this.id;
	}
	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	
	
}
