package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table( name="TELEFONES")
public class Telefones extends AbstractEntity {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID_TELEFONE")
	@GeneratedValue(generator = "TELEFONES_SEQ", strategy = GenerationType.SEQUENCE )
	@SequenceGenerator( allocationSize = 1, name = "TELEFONES_SEQ", sequenceName = "TELEFONES_SEQ" )
	private Integer id;
	
	@Column( name = "numero", length = 100, nullable = false)
	private String numero;
	
	@Enumerated(EnumType.STRING)
	private TipoTelefone tipo;
	
	@ManyToMany( cascade = CascadeType.ALL)
	@JoinTable( name= "telefones_x_clientes", 
					joinColumns = {
						@JoinColumn( name = "id_telefone" )}, 
						inverseJoinColumns = { @JoinColumn( name = "id_cliente")})
	private List<Clientes> clientes = new ArrayList<>();
	
	public TipoTelefone getTipo() {
		return tipo;
	}
	public void setTipo(TipoTelefone tipo) {
		this.tipo = tipo;
	}
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	@Override
	public Integer getId() {
		return this.id;
	}
	@Override
	public void setId(Integer id) {
		this.id = id;
	} 
}
