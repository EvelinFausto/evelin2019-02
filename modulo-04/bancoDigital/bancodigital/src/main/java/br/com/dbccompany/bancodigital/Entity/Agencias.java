package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Agencias extends AbstractEntity{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID_AGENCIA")
	@GeneratedValue(generator = "AGENCIAS_SEQ", strategy = GenerationType.SEQUENCE )
	@SequenceGenerator( allocationSize = 1, name = "AGENCIAS_SEQ", sequenceName = "AGENCIAS_SEQ" )
	private Integer id;
	
	@Column( name = "codigo", length = 100, nullable = false)
	private String codigo;
	
	@Column( name = "nome", length = 100, nullable = false)
	private Integer nome;
	
	
	@ManyToOne
	@JoinColumn( name = "fk_id_endereco" )
	private Enderecos enderecoAgencia;
	
	@ManyToOne
	@JoinColumn( name = "fk_id_banco" )
	private Bancos banco;
	
	@ManyToMany( cascade = CascadeType.ALL)
	@JoinTable( name= "agencias_x_correntistas", 
					joinColumns = {
						@JoinColumn( name = "id_agencia" )}, 
						inverseJoinColumns = { @JoinColumn( name = "id_correntista")})
	private List<Correntistas> correntistas = new ArrayList<>();

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Integer getNome() {
		return nome;
	}

	public void setNome(Integer nome) {
		this.nome = nome;
	}

	public Enderecos getEndereco() {
		return enderecoAgencia;
	}

	public void setEndereco(Enderecos endereco) {
		this.enderecoAgencia = endereco;
	}

	public Bancos getBanco() {
		return banco;
	}

	public void setBanco(Bancos banco) {
		this.banco = banco;
	}

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
		
	}
	
}

