package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigita.Dto.EstadosDTO;
import br.com.dbccompany.bancodigital.Entity.Estados;

public class EstadosDAO extends AbstractDAO<Estados> {
	
	public Estados parseFrom(EstadosDTO dto) {
		Estados estados = null;
		if(dto.getIdEstados() != null) {
			estados = buscar(dto.getIdEstados());
		}
		estados.setNome(dto.getNome());
		return estados;
	}

	@Override
	protected Class<Estados> getEntityClass(){
		return Estados.class;
	}
}
