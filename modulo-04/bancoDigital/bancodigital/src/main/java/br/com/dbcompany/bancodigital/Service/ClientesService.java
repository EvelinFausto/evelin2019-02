package br.com.dbcompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.ClientesDAO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Clientes;

public class ClientesService {
	
	private static final ClientesDAO CLIENTES_DAO = new ClientesDAO();
	private static final Logger LOG = Logger.getLogger(ClientesService.class.getName());

	public void salvarPaises(Clientes clientes ) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		try {
			Clientes clientesRes = CLIENTES_DAO.buscar(1);
			if (clientesRes == null) {
				CLIENTES_DAO.criar(clientes);
			}else {
				clientes.setId(clientesRes.getId());
				CLIENTES_DAO.atualizar(clientes);
			}
			
			if(started) {
				transaction.commit();
			}
		}catch( Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
}
