package br.com.dbccompany.bancodigita.Dto;

public class BairrosDTO {
	
	private Integer idBairro;
	private String nome;
	
	private EstadosDTO estados;

	public Integer getIdBairro() {
		return idBairro;
	}

	public void setIdBairro(Integer idBairro) {
		this.idBairro = idBairro;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public EstadosDTO getEstados() {
		return estados;
	}

	public void setEstados(EstadosDTO estados) {
		this.estados = estados;
	}
	
}
