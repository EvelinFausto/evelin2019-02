package br.com.dbccompany.bancodigita.Dto;

public class EstadosDTO {

	private Integer idEstado;
	private String nome;
	
	private PaisesDTO paises;

	public Integer getIdEstados() {
		return idEstado;
	}

	public void setIdEstados(Integer idEstado) {
		this.idEstado = idEstado;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public PaisesDTO getPaises() {
		return paises;
	}

	public void setPaises(PaisesDTO paises) {
		this.paises = paises;
	}
	
}
