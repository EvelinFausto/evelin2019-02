package br.com.dbcompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.CorrentistasDAO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Correntistas;

public class CorrentistasService {
	
	private static final CorrentistasDAO CORRENTISTAS_DAO = new CorrentistasDAO();
	private static final Logger LOG = Logger.getLogger(CorrentistasService.class.getName());

	public void salvarPaises(Correntistas correntistas ) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		try {
			Correntistas paisesRes = CORRENTISTAS_DAO.buscar(1);
			if (paisesRes == null) {
				CORRENTISTAS_DAO.criar(correntistas);
			}else {
				correntistas.setId(paisesRes.getId());
				CORRENTISTAS_DAO.atualizar(correntistas);
			}
			if(started) {
				transaction.commit();
			}
		}catch( Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
}
