package br.com.dbccompany.bancodigita.Dto;

public class EnderecosDTO {

	private Integer idEndereco;
	private String logradouro;
	private Integer numero;
	private String complemento;
	
	private BairrosDTO bairros;

	public Integer getIdEndereco() {
		return idEndereco;
	}

	public void setIdEndereco(Integer idEndereco) {
		this.idEndereco = idEndereco;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public BairrosDTO getBairros() {
		return bairros;
	}

	public void setBairros(BairrosDTO bairros) {
		this.bairros = bairros;
	}

	
}
