package br.com.dbccompany.bancodigital.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Enderecos extends AbstractEntity{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "ENDERECO_SEQ", strategy = GenerationType.SEQUENCE )
	@Column(name = "ID_ENDERECO")
	@SequenceGenerator( allocationSize = 1, name = "ENDERECO_SEQ", sequenceName = "ENDERECO_SEQ" )
	private Integer id;
	
	@Column( name = "logradouro", length = 100, nullable = false)
	private String logradouro;
	
	@Column( name = "numero", length = 100, nullable = false)
	private Integer numero;
	
	@Column( name = "complemento", length = 100, nullable = true)
	private String complemento;
	
	@OneToMany(mappedBy = "enderecoAgencia", cascade = CascadeType.ALL)
	private List<Agencias> agencias = new ArrayList<>();
	
	@OneToMany(mappedBy = "enderecoCliente", cascade = CascadeType.ALL)
	private List<Clientes> clientes = new ArrayList<>();
	
	
	@ManyToOne
	@JoinColumn( name = "fk_id_bairro" )
	private Bairros bairro;

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Bairros getBairro() {
		return bairro;
	}

	public void setBairro(Bairros bairro) {
		this.bairro = bairro;
	}

	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}
	
	
}

