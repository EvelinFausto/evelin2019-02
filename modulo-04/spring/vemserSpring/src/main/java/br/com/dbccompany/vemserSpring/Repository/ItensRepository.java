package br.com.dbccompany.vemserSpring.Repository;

import br.com.dbccompany.vemserSpring.Entity.Itens;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItensRepository extends CrudRepository<Itens, Integer> {

    Itens findByDescricao( String descricao);
}
