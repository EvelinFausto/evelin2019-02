package br.com.dbccompany.vemserSpring.Controller;

import br.com.dbccompany.vemserSpring.Entity.InventarioItem;
import br.com.dbccompany.vemserSpring.Service.InventarioXItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/inventarioItem")
public class InventarioItemController {

    @Autowired
    InventarioXItemService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<InventarioItem> todosInventariosItens() {
        return service.todosInventarioItem();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public InventarioItem novoInventarioItem(@RequestBody InventarioItem inventarioItem) {
        return service.salvar(inventarioItem);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public InventarioItem editarInventarioItem( @PathVariable Integer id, @RequestBody InventarioItem inventarioItem) {
        return service.editar(id, inventarioItem);
    }

    @GetMapping( value = "/deletar/{id}" )
    @ResponseBody
    public List<InventarioItem> deletarInventarioItem(@PathVariable Integer id){
        service.deletarInventarioItem(id);
        return service.todosInventarioItem();
    }
}


