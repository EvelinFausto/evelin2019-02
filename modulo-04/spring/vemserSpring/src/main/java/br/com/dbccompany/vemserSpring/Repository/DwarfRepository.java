package br.com.dbccompany.vemserSpring.Repository;

import br.com.dbccompany.vemserSpring.Entity.Dwarf;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DwarfRepository extends CrudRepository<Dwarf, Integer> {

    Dwarf findByDano( Double dano );
}