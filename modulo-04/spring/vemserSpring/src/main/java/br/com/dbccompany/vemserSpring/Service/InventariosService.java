package br.com.dbccompany.vemserSpring.Service;

import br.com.dbccompany.vemserSpring.Entity.Inventarios;
import br.com.dbccompany.vemserSpring.Repository.InventariosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class InventariosService {

    @Autowired
    private InventariosRepository inventariosRepository;

    @Transactional(rollbackFor = Exception.class)
    public Inventarios salvar(Inventarios inventario ){
        return inventariosRepository.save(inventario);
    }

    @Transactional(rollbackFor = Exception.class)
    public Inventarios editar( Integer id, Inventarios inventario ){
        inventario.setId(id);
        return inventariosRepository.save(inventario);
    }

    public List<Inventarios> todosInventarios(){
        return (List<Inventarios>) inventariosRepository.findAll();
    }

    public void deletar(Integer id){
        inventariosRepository.deleteById(id);
    }
}
