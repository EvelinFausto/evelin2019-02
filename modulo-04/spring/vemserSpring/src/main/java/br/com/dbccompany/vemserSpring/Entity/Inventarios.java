package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Inventarios {

    @Id
    @Column(name = "idInventario")
    @GeneratedValue( generator = "INVENTARIOS_SEQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(allocationSize = 1, name = "INVENTARIOS_SEQ", sequenceName = "INVENTARIOS_SEQ")
    private Integer id;

    @Column(name = "quantidade")
    private Integer quantidade;

    @OneToMany(mappedBy = "inventarios", cascade = CascadeType.ALL)
    private List<InventarioItem> inventarioItem = new ArrayList<>();

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getId() {

        return id;
    }

    public void setId(Integer id) {

        this.id = id;
    }

    public List<InventarioItem> getInventarioItem() {

        return inventarioItem;
    }

    public void setInventarioItem(List<InventarioItem> inventarioItem) {

        this.inventarioItem = inventarioItem;
    }
}
