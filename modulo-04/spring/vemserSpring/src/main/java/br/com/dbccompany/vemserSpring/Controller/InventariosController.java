package br.com.dbccompany.vemserSpring.Controller;

import br.com.dbccompany.vemserSpring.Entity.Inventarios;
import br.com.dbccompany.vemserSpring.Service.InventariosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/inventarios")
public class InventariosController {

    @Autowired
    InventariosService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Inventarios> todosInventarios() {
        return service.todosInventarios();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Inventarios novoInventario(@RequestBody Inventarios inventarios) {
        return service.salvar(inventarios);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Inventarios editarInventario( @PathVariable Integer id, @RequestBody Inventarios inventario) {
        return service.editar(id, inventario);
    }

    @GetMapping( value = "/deletar/{id}" )
    @ResponseBody
    public List<Inventarios> deletarInventario(@PathVariable Integer id){
        service.deletar(id);
        return service.todosInventarios();
    }
}
