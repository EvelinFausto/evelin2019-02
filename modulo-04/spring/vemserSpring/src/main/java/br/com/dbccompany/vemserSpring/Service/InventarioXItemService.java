package br.com.dbccompany.vemserSpring.Service;

import br.com.dbccompany.vemserSpring.Entity.InventarioItem;
import br.com.dbccompany.vemserSpring.Entity.Inventarios;
import br.com.dbccompany.vemserSpring.Entity.Itens;
import br.com.dbccompany.vemserSpring.Repository.InventarioXItemRepository;
import br.com.dbccompany.vemserSpring.Repository.InventariosRepository;
import br.com.dbccompany.vemserSpring.Repository.ItensRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class InventarioXItemService {

    @Autowired
    private InventarioXItemRepository InventarioXItemRepository;

    @Autowired
    private ItensRepository itensRepository;

    @Autowired
    private InventariosRepository inventariosRepository;

    @Transactional(rollbackFor = Exception.class)
    public InventarioItem salvar(InventarioItem inventarioItem ){
        Itens itensObject = inventarioItem.getItens();
        Inventarios inventariosObject = inventarioItem.getInventarios();

        if(itensObject.getId() == null) {
            itensObject = itensRepository.save(itensObject);
        }

        if(inventariosObject.getId() == null) {
            inventariosObject = inventariosRepository.save(inventariosObject);
        }

        inventarioItem.setInventarios(inventariosObject);
        inventarioItem.setItens(itensObject);

        return InventarioXItemRepository.save(inventarioItem);


    }

    @Transactional(rollbackFor = Exception.class)
    public InventarioItem editar( Integer id, InventarioItem inventarioItem ){
        inventarioItem.setId(id);
        return InventarioXItemRepository.save(inventarioItem);
    }

    public List<InventarioItem> todosInventarioItem(){
        return (List<InventarioItem>) InventarioXItemRepository.findAll();
    }

    public void deletarInventarioItem(Integer id){
        InventarioXItemRepository.deleteById(id);
    }

}
