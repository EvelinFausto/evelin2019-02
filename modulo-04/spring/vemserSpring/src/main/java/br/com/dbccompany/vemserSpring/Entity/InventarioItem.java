package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.*;

@Entity
public class InventarioItem {
    @Id
    @Column(name = "idInventarioItem", nullable = false)
    @GeneratedValue( generator = "INVENTARIO_ITEM_SEQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(allocationSize = 1, name = "INVENTARIO_ITEM_SEQ", sequenceName = "INVENTARIO_ITEM_SEQ")
    private Integer id;

    @Column(name = "quantidade", nullable = false)
    private Integer quantidade;

    @Column(name = "equipado", nullable = false)
    private Boolean equipado;

    @ManyToOne
    @JoinColumn(name = "fk_id_inventario" )
    private Inventarios inventarios;

    @ManyToOne
    @JoinColumn(name = "fk_id_item" )
    private Itens itens;

    public Itens getItens() {
        return itens;
    }

    public void setItens(Itens itens) {
        this.itens = itens;
    }

    public Inventarios getInventarios() {
        return inventarios;
    }

    public void setInventarios(Inventarios inventarios) {
        this.inventarios = inventarios;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getEquipado() {
        return equipado;
    }

    public void setEquipado(Boolean equipado) {
        this.equipado = equipado;
    }
}
