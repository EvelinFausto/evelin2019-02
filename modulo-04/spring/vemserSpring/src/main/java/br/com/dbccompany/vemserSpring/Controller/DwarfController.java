package br.com.dbccompany.vemserSpring.Controller;

import br.com.dbccompany.vemserSpring.Entity.Dwarf;
import br.com.dbccompany.vemserSpring.Service.DwarfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/dwarf")
public class DwarfController {

    @Autowired
    DwarfService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Dwarf> todosDwarf() {
        return service.todosDwarf();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Dwarf novoDward(@RequestBody Dwarf dwarf) {
        return service.salvar(dwarf);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Dwarf editarDwarf( @PathVariable Integer id, @RequestBody Dwarf dwarf) {
        return service.editar(id, dwarf);
    }

    @GetMapping( value = "/deletar/{id}" )
    @ResponseBody
    public List<Dwarf> deletarDwarf(@PathVariable Integer id){
        service.deletarDwarf(id);
        return service.todosDwarf();
    }

}

