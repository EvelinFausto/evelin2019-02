package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Itens {

    @Id
    @Column(name = "idItem")
    @GeneratedValue( generator = "ITENS_SEQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(allocationSize = 1, name = "ITENS_SEQ", sequenceName = "ITENS_SEQ")
    private Integer id;

    @Column(name = "descricao")
    private String descricao;

    @OneToMany(mappedBy = "itens", cascade = CascadeType.ALL)
    private List<InventarioItem> inventarioItem = new ArrayList<>();

    public List<InventarioItem> getInventarioItem() {
        return inventarioItem;
    }

    public void setInventarioItem(List<InventarioItem> inventarioItem) {
        this.inventarioItem = inventarioItem;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
