package br.com.dbccompany.vemserSpring.Controller;

import br.com.dbccompany.vemserSpring.Entity.Itens;
import br.com.dbccompany.vemserSpring.Service.ItensService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/itens")
public class ItensController {

    @Autowired
    ItensService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Itens> todosItens() {
        return service.todosItens();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Itens novoItem(@RequestBody Itens itens) {
        return service.salvar(itens);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Itens editarItem( @PathVariable Integer id, @RequestBody Itens itens) {
        return service.editar(id, itens);
    }

    @GetMapping( value = "/deletar/{id}" )
    @ResponseBody
    public List<Itens> deletarItem(@PathVariable Integer id){
        service.deletarItens(id);
        return service.todosItens();
    }
}

