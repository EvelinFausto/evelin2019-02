package br.com.dbccompany.vemserSpring.Service;

import br.com.dbccompany.vemserSpring.Entity.Itens;
import br.com.dbccompany.vemserSpring.Repository.ItensRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ItensService {

    @Autowired
    private ItensRepository itensRepository;

    @Transactional(rollbackFor = Exception.class)
    public Itens salvar(Itens itens ){

        return itensRepository.save(itens);
    }

    @Transactional(rollbackFor = Exception.class)
    public Itens editar( Integer id, Itens itens ){
        itens.setId(id);
        return itensRepository.save(itens);
    }

    public List<Itens> todosItens(){
        return (List<Itens>) itensRepository.findAll();
    }

    public void deletarItens(Integer id){
        itensRepository.deleteById(id);
    }
}
