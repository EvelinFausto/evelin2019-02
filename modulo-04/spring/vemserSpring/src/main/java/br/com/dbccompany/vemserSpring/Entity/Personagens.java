package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Personagens {
    @Id
    @Column(name = "idPersonagem")
    @GeneratedValue( generator = "PERSONAGENS_SEQ", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(allocationSize = 1, name = "PERSONAGENS_SEQ", sequenceName = "PERSONAGENS_SEQ")
    private Integer id;

    @Column(name = "nome", length = 100, nullable = true)
    private String nome;

    @Column(name = "vida", nullable = true)
    private Double vida;

    @Column(name = "experiencia", nullable = true)
    private Double experiencia;

    @Column(name = "dano", nullable = true)
    private Double dano;

    @Enumerated(EnumType.STRING)
    private Status status;

    @Enumerated(EnumType.STRING)
    private TipoPersonagem tipo;


    public Integer getId() {

        return id;
    }

    public void setId(Integer id) {

        this.id = id;
    }

    public TipoPersonagem getTipo() {

        return tipo;
    }

    protected void setTipo(TipoPersonagem tipo) {

        this.tipo = tipo;
    }

    public Status getStatus() {

        return status;
    }

    public void setStatus(Status status) {

        this.status = status;
    }

    public Double getExperiencia() {

        return experiencia;
    }

    public void setExperiencia(Double experiencia) {

        this.experiencia = experiencia;
    }

    public Double getVida() {

        return vida;
    }

    public void setVida(Double vida) {

        this.vida = vida;
    }

    public String getNome() {

        return nome;
    }

    public void setNome(String nome) {

        this.nome = nome;
    }

    public Double getDano() {

        return dano;
    }

    public void setDano(Double dano) {

        this.dano = dano;
    }
}