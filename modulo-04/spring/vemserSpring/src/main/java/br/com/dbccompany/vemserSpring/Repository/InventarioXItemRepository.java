package br.com.dbccompany.vemserSpring.Repository;

import br.com.dbccompany.vemserSpring.Entity.InventarioItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InventarioXItemRepository extends CrudRepository<InventarioItem, Integer> {
    InventarioItem findByEquipado( Boolean equipado);
}
