import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import {PrivateRoute} from './components/privateRoute';
import Login from './paginas/login'
import paginaInicial from './paginas/paginaInicial'
import elfos from './paginas/todosElfo'

class App extends Component {
  constructor ( props ) {
    super( props )
    this.state = {
    }
  }


  render() {
    return (
      <div className="App">
        <div className="app-Header">
          <Router>
            <React.Fragment>
            <section>
              <PrivateRoute path="/paginaInicial" component={paginaInicial} />
              <Route path="/login" component={Login} />
              <PrivateRoute path="/elfos" component={elfos} />
            </section>
          </React.Fragment>
          </Router>
        </div>
      </div>
    );
  }
  return
}

export default App;
