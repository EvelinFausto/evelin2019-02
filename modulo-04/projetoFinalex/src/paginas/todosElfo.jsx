import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Header from '../components/header'
import * as axios from 'axios';



class todosElfos extends Component {

  constructor ( props ) {
    super( props )
    this.state = {
      elfos: '',
      autorizacao: this.autorizacao()
    }
  }

  autorizacao(){
    return (
      axios.get('http://localhost:8080/api/elfo/', {
        headers:{
          Authorization : localStorage.getItem('Authorization')
        }
      })
    )
  }

    trocaValoresState(evt) {
      const value = evt.target.value;
        this.setState({
          tt:value
        })
    }

    render() {
        return (
          <div >
            <Router>
              <React.Fragment>
                <Header/>
                <section>
                  <div className="container">
                    <h1 className="titulo">Elfos</h1>
                    <button type="button" onClick={this.autorizacao()}>listar</button>
                  </div>
                </section>
              </React.Fragment>
            </Router>
          </div>
        );
      }
      return
}



export default todosElfos;