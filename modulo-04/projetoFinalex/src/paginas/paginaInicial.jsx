import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Header from './../components/header'

class PaginaInicial extends Component {
    render() {
        return (
          <div >
            <Router>
              <React.Fragment>
                <section>
                  <Header/>
                  <h1>Seja bem-vindo</h1>
                </section>
              </React.Fragment>
            </Router>
          </div>
        );
      }
      return
}



export default PaginaInicial;