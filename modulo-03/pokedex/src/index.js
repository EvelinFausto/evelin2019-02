/* eslint-disable no-alert */
const pokeApi = new PokeApi();
// let pokemonEspecifico = pokeApi.buscarEspecifico(12);
// pokemonEspecifico.then(pokemon => {
//     let poke = new Pokemon( pokemon );
//     rederizacaoPokemon( poke );
// })

function rederizacaoPokemon( pokemon ) {
  const dadosPokemon = document.getElementById( 'dadosPokemon' );
  const nome = dadosPokemon.querySelector( '.nome' );
  nome.innerHTML = pokemon.nome;
  dadosPokemon.querySelector( '.id' ).innerHTML = pokemon.id;
  const altura = dadosPokemon.querySelector( '.altura' );
  altura.innerHTML = `${ pokemon.altura }0 cm`;
  const peso = dadosPokemon.querySelector( '.peso' );
  peso.innerHTML = `${ pokemon.peso / 10 }Kg`;
  const tipo = dadosPokemon.querySelector( '.tipo' );
  const lista = [];
  for ( let i = 0; i < pokemon.tipo.length; i += 1 ) {
    lista.push( pokemon.tipo[i].type.name );
  }
  tipo.innerHTML = `<li> ${ lista.join( '</li><li>' ) }</li>`;
  const estatistica = dadosPokemon.querySelector( '.estatistica' );
  const listaEstatistica = [];
  for ( let i = 0; i < pokemon.estatistica.length; i += 1 ) {
    listaEstatistica.push( `${ pokemon.estatistica[i].stat.name }:${ pokemon.estatistica[i].base_stat }%` );
  }
  estatistica.innerHTML = `<li> ${ listaEstatistica.join( '</li><li>' ) }</li>`;

  const foto = document.getElementById( 'foto' );
  foto.src = pokemon.foto.front_default;
}

const campo = document.getElementById( 'idDigitado' );
function verificaCampo( idDigitado ) {
  if ( ( idDigitado === '' ) || ( idDigitado <= 0 ) || ( idDigitado > 802 ) ) {
    return false;
  }
  return true;
}

function chamandoBuscarEspecifico( id ) {
  const resultado = pokeApi.buscarEspecifico( id );
  resultado.then( pokemon => {
    const poke = new Pokemon( pokemon );
    rederizacaoPokemon( poke );
  } )
}

campo.addEventListener( 'blur', () => {
  const idDigitado = campo.value;
  const verifica = verificaCampo( idDigitado );
  if ( !verifica ) {
    document.getElementById( 'error' ).innerHTML = 'Digite um ID válido'
  } else {
    chamandoBuscarEspecifico( idDigitado );
  }
} );

document.getElementById( 'botao' ).addEventListener( 'click', () => {
  const numero = Math.floor( Math.random() * 802 );
  chamandoBuscarEspecifico( numero );
} )

// vet listaJaVistos = []
// sessionStorage.setItem( numero, listaJaVistos ); salvar valores
// sessionStorage.getItem('name'); obter valores
