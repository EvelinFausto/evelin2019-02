const pi = 3.14;

function calcularCirculo(tipo, raio){
    let resultado = "0";
    if(tipo == "a"){
        resultado = pi * (raio*raio);
    }
    if(tipo == "c"){
        resultado = (raio*2) * pi;
    }
     console.log(resultado);
}

calcularCirculo("a",5);

function naoBissexto(ano){
    if ( ( ano % 4 == 0 && ano % 100 != 0 ) || (ano % 400 == 0) ) { 
        console.log(`${ano} é bissexto`); 
    } else {
        console.log(`${ano} não é bissexto`);
    }
}

naoBissexto(2020);

const array = [ 1, 56, 4.34, 6, -2 ];

function somarPares(array){
    const [n0, , n2, , n4] = array
    console.log(n0 + n2 + n4);

}

somarPares(array);

function adicionar(n1){
    return function(n2){
        return n1 + n2;
    }
}
let soma = adicionar(2)(4);

console.log(soma);


function float2moeda(num) {

    x = 0;
 
    if(num<0) {
       num = Math.abs(num);
       x = 1;
    }
    if(isNaN(num)) num = "0";
      cents = Math.floor((num*100+0.5)%100);

   num = Math.floor((num*100+0.5)/100).toString();

   if(cents < 10) cents = "0" + cents;
      for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
         num = num.substring(0,num.length-(4*i+3))+'.'
               +num.substring(num.length-(4*i+3));
               ret = num + ',' + cents;
               if (x == 1) ret = ' - ' + ret;return ret;
}

let tt = float2moeda(12);
console.log(tt);

// function calcularCirculoSor({raio, tipoCalculo:tipo}){
//     return Math.ceil(tipo == "A" ?  Math.Pi *(raio,2): )
// }

// let circulo = {
//     raio : 3,
//     tipoCalculo: "A"
// }

// console.log(calcularCirculoSor(circulo));

// function naoBissextoSor(ano){
//     return (ano %400 == 0) || (ano %4 ==0 && ano %100 !==0) ? false : true;
// }

//let naoBissextoSor = ano => (ano %400 == 0) || (ano %4 ==0 && ano %100 !==0) ? false : true;

// const testes = {
//     diaAula : "Segundo",
//     local : "DBC",
//     naoBissextoSor(ano){
//         return (ano %400 == 0) || (ano %4 ==0 && ano %100 !==0) ? false : true;
//     }
//}
// console.log(testes.naoBissextoSor(2016));

function somarParesSor(numeros){
    let resultado = 0;
    for (let i = 0; i < numero.length; i++){
        if(i % 2 == 0){
            resultado += numero[i];
        }
    }
    return resultado;
}

// console.log(somarParesSor([1,56,4.34,6,-2]));

let adicionarSor = op1 => op2 => op1 + op2

// console.log(adicionarSor(3)(4));
// console.log(adicionarSor(5642)(8749));

// const is_divisivel = (divisor, numero) => !(numero % divisor);
// const divisor = 2;
// console.log(is_divisivel(divisor, 20));
// console.log(is_divisivel(divisor, 11));
// console.log(is_divisivel(divisor, 12));

const divisivelPor = divisor => numero => !(numero % divisor);
const is_divisivel = divisivelPor(2);
// console.log(is_divisivel(20));
// console.log(is_divisivel(11));
// console.log(is_divisivel(12));

function arredondar(numero, precisao = 2){
    const fator = Math.pow( 10, precisao)
    return Math.ceil( numero * fator) / fator
}

function imprimeBRL(numero){
    let qtdCasasMilhares = 3;
    let separadorMilhar ='.';
    let separadorDecimal =',';

    let stringBuffer = []
    let parteDecimal = arredondar(Math.abs(numero)%1)
    let parteInteira = Math.trunc(numero)
    let parteInteiraString = Math.abs(parteInteira).toString()
    let parteInteiraTamanho = parteInteiraString.length

    let c = 1 
    while(parteInteiraString.length > 0){
        if(c % qtdCasasMilhares == 0){
            stringBuffer.push(`${separadorMilhar}${parteInteiraString.slice( parteInteiraTamanho - c)}`)
            parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho - c)
        } else if(parteInteiraString.length < qtdCasasMilhares){
            stringBuffer.push(parteInteiraString)
            parteInteiraString = ''
        }
        c++
    }
    stringBuffer.push(parteInteiraString)
    let decimalString = parteDecimal.toString().replace('0.', '').padStart(2,'')
    return `${parteInteira >= 0 ? 'R$': '-R$'}${stringBuffer.reverse().join('') }${separadorDecimal}${decimalString}`
}
console.log(imprimeBRL(0));
console.log(imprimeBRL(3498.99));
console.log(imprimeBRL(-3498.99));
console.log(imprimeBRL(123111.99));




