import React, {Component} from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import {Link } from 'react-router-dom';
import img from './../imgs/logo.png'
import '../css/header.css'

export default class Header extends Component {

    logout() {
        localStorage.removeItem('Authorization');
      }

    render() {
        return (
            <Router>
                <React.Fragment>
                <div className="container">
                    <nav className="">
                        <div className="row">
                            <div className="logo float-left">
                                <img alt="logo" src={img} ></img>
                            </div>
                            <ul className="text-center float-right">
                                <li><Link className="test" to="./login">Login</Link></li>
                                <li><Link to="./paginaInicial">Home</Link></li>  
                                <li><Link to="./agencias">Agências</Link></li>                  
                                <li><Link to="./cliente">Clientes</Link></li>                  
                                <li><Link to="./tiposConta">Tipos de contas</Link></li>                  
                                <li><Link to="./contasCliente">Contas de cliente</Link></li> 
                                <li><button type="button" onClick={ this.logout.bind(this)}>Deslogar</button></li>                 
                            </ul>
                        </div>
                    </nav>
                </div>
            </React.Fragment>
        </Router>
        )
    }
}