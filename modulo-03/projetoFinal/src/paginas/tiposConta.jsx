import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import ListaTiposConta from './../dados/listaTiposContas';
import Header from './../components/header'

class TiposConta extends Component {
  constructor ( props ) {
    super( props )
    this.listaTiposConta = new ListaTiposConta();
    this.state = {
      listaTiposConta: this.listaTiposConta.listarTodoTipoConta,
      listTiposConts:[],
      validacao:false,
      tipo:'',
      codigo: ''
    }
  }

  formatarLista(){
    const list = this.state.listaTiposConta
    let lista = []
    for (let i=0; i<list.length;i++){
      lista.push(list[i])
    }
    const listItems = lista.map((tipoContas) =>
      <button key={tipoContas} onClick={() => this.carregarDadosContas(tipoContas)}>{tipoContas}</button>
    );
    this.setState({
      listTiposConts: listItems
    })
  }

  carregarDadosContas(tiposConta){
    const funcaoLista = this.listaTiposConta.listarDadosTiposContas(tiposConta)
    this.setState({
      tipo: funcaoLista.tipo,
      codigo: funcaoLista.codigo,
      validacao: true
    })
  }  

  dadosConta(){
    return this.state.validacao && (
    <div>
      {
        <div>
        <h1>Conta: {this.state.tipo} | Código: {this.state.codigo}</h1>
        </div>
      }
    </div>
    )
  }

    render() {
        return (
          <div >
            <Router>
              <React.Fragment>
              <Header/>
                <section>
                  <h1>Tipos de contas</h1>
                  <button onClick={() => this.formatarLista()}>Procurar tipos de contas</button>
                  {this.state.listTiposConts}
                  {this.dadosConta()}
                </section>
              </React.Fragment>
            </Router>
          </div>
        );
      }
      return
}



export default TiposConta;