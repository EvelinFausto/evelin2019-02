import React, { Component } from 'react';
import * as axios from 'axios';

export default class Login extends Component{
    constructor(props) {
        super(props)
        this.state = {
            email:'',
            senha:''
        }
        this.trocaValoresState = this.trocaValoresState.bind( this )
    }

    trocaValoresState(e) {
        const { name, value } = e.target;
        this.setState({
            [name] : value
        })
    }

    logar(e) {
        e.preventDefault();
        const { email, senha } = this.state
        if (email && senha) {
            //executo a regra de login
            axios.post('http://localhost:1337/login', {
                email: this.state.email,
                senha: this.state.senha,
            }).then( resp => {
                localStorage.setItem('Authorization', resp.data.token );
                this.props.history.push("/paginaInicial")
            }
            )
        }
    }   

    render() {
        return (
            <React.Fragment>
                <h5>Logar</h5>
                <input type="text" name="email" id="email" placeholder="Digite o email" onChange={this.trocaValoresState} />
                <input type="password" name="senha" id="senha" placeholder="Digite sua senha" onChange={this.trocaValoresState} />
                <button type="button" onClick={ this.logar.bind( this ) }>Logar</button>
            </React.Fragment>
        )
    }
}