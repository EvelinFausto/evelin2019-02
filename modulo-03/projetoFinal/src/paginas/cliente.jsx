import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import ListaClientes from './../dados/listaClientes'
import Header from './../components/header'

class Cliente extends Component {

  constructor ( props ) {
    super( props )
    this.ListaClientes = new ListaClientes();
    this.state = {
      listarTodosClientes: this.ListaClientes.listarTodosClientes,
      listClients:[],
      validacao:false,
      nome:'',
      cpf: '',
      endereco: '',
      telefone: '',
      email:''
    }
  }

  formatarLista(){
    const list = this.state.listarTodosClientes
    let lista = []
    for (let i=0; i<list.length;i++){
      lista.push(list[i])
    }
    const listItems = lista.map((cliente) =>
      <button key={cliente} onClick={() => this.carregarDadosCliente(cliente)}>{cliente}</button> 
    );
    this.setState({
      listClients: listItems
    })
  }

  carregarDadosCliente(cliente){
    const funcaoLista = this.ListaClientes.listarDadosDoCliente(cliente)
    this.setState({
      nome: funcaoLista.nome,
      cpf: funcaoLista.cpf,
      endereco: funcaoLista.endereco,
      telefone: funcaoLista.telefone,
      email: funcaoLista.email,
      validacao: true
    })
  }  

  dadosCliente(){
    return this.state.validacao && (
    <div>
      {
        <div>
        <h1>Nome: {this.state.nome} | CPF: {this.state.cpf}</h1>
        <h3>Endereço: {this.state.endereco} | Telefone: {this.state.telefone}</h3>
        <h3>Email: {this.state.email}</h3>
        </div>
      }
    </div>
    )
  }

    render() {
        return (
          <div >
            <Router>
              <React.Fragment>
                <section>
                  <Header/>
                  <h1>Clientes</h1>
                  <button onClick={() => this.formatarLista()}>Procurar Cliente</button>
                  {this.state.listClients}
                  {this.dadosCliente()}
                </section>
              </React.Fragment>
            </Router>
          </div>
        );
      }
      return
}



export default Cliente;