import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Header from '../components/header'
import * as axios from 'axios';



class Agencia extends Component {

  constructor ( props ) {
    super( props )
    this.state = {
      listaAgencias: '',
      todosDados: this.carregarDadosAgencia(),
      validacao: false,
      codigo: '',
      endereco: '',
      id: '',
      cod: '',
      tt: ''
    }
  }

  autorizacao(){
    return (
      axios.get('http://localhost:1337/agencias', {
        headers:{
          Authorization : localStorage.getItem('Authorization')
        }
      })
    )
  }

  listar(e) {
    e.preventDefault();
    const tudo = this.state.todosDados    
      const lista = tudo.map((ag) => 
      <button key={ag.id-1} onClick={() => this.dados(ag.id-1)}>{ag.nome}</button>
      )
    this.setState({
      listaAgencias: lista
    })
  }

  dados(id){
    const agencia = this.state.todosDados[id]
    this.setState({
      nome: agencia.nome,
      codigo:agencia.codigo ,
      endereco:agencia.endereco,
      id: agencia.id,
      validacao: true
    })
  }

  carregarDadosAgencia(){
    this.autorizacao().then( resp => {
      const agencias = resp.data.agencias
      this.setState({
        todosDados: agencias
      })
    });
    }

    dadosAgencia(){
      return this.state.validacao && (
      <div>
        {
          <div>
          <h1>Agência: {this.state.nome} | Código: {this.state.codigo}  </h1>
          <h1>Endereco: {this.state.endereco.logradouro}, {this.state.endereco.numero} </h1>
          <h1>Bairro: {this.state.endereco.bairro} | Cidade: {this.state.endereco.cidade} {this.state.endereco.uf } </h1>
          </div>
        }
      </div>
      )
    }

    trocaValoresState(evt) {
      const value = evt.target.value;
        this.setState({
          tt:value
        })
    }

    render() {
        return (
          <div >
            <Router>
              <React.Fragment>
                <Header/>
                <section>
                  <div className="container">
                    <h1 className="titulo">Agências</h1>
                    <button type="button" onClick={ this.listar.bind( this ) }>listar</button>
                    {this.state.listaAgencias}
                    {this.dadosAgencia()}
                    <div>
                      <label>Agência</label><input type="text" /><button>Buscar</button>
                    </div>
                    <div>
                      <label>Código</label><input type="number" name="cod" maxLength="3" onChange={this.trocaValoresState} /><button >Buscar</button>
                    </div>
                  </div>
                </section>
              </React.Fragment>
            </Router>
          </div>
        );
      }
      return
}



export default Agencia;