import Clientes from "./cliente";

export default class ListaClientes {
    constructor() {
        this.clientes = [
            { nome: 'Evelin Fausto', cpf:'432.432.432-34', endereco:'av.Arena do Grêmio, 12', telefone:'51 98243-5353', email:'evelin@gmail.com' },
            { nome: 'Renato Portaluppi', cpf:'111.111.111-11', endereco:'av.Arena do Grêmio, 7', telefone:'51 98243-5353', email:'renato@gmail.com' },
            { nome: 'Jean Pyerre', cpf:'222.222.222-22', endereco:'av.Arena do Grêmio, 21', telefone:'51 98243-5353', email:'jean@gmail.com' },
            { nome: 'Everton Cebolinha', cpf:'333.333.333-33', endereco:'av.Arena do Grêmio, 11', telefone:'51 98243-5353', email:'cebolinha@gmail.com' },
            { nome: 'Geromito', cpf:'444.444.444-44', endereco:'av.Arena do Grêmio, 3', telefone:'51 98243-5353', email:'geromito@gmail.com' },
            { nome: 'Kannemann', cpf:'555.555.555-55', endereco:'av.Arena do Grêmio, 4', telefone:'51 98243-5353', email:'kannemann@gmail.com' },
            { nome: 'Matheus Henrique', cpf:'666.666.666-66', endereco:'av.Arena do Grêmio, 14', telefone:'51 98243-5353', email:'matheus@gmail.com' },
            { nome: 'Léo moura', cpf:'777.777.777-77', endereco:'av.Arena do Grêmio, 2', telefone:'51 98243-5353', email:'moura@gmail.com' },
            { nome: 'Luan Guilherme', cpf:'888.888.888-88', endereco:'av.Arena do Grêmio, 7', telefone:'51 98243-5353', email:'luan@gmail.com' },
            { nome: 'Bruno Cortez', cpf:'999.999.999-99', endereco:'av.Arena do Grêmio, 12', telefone:'51 98243-5353', email:'cortez@gmail.com' },
            { nome: 'Alisson', cpf:'545.545.434-33', endereco:'av.Arena do Grêmio, 23', telefone:'51 98243-5353', email:'alisson@gmail.com' },

        ].map( c => new Clientes(c.nome, c.cpf, c.endereco, c.telefone, c.email ))
    }

    get listarTodosClientes() {
        const todosClientes = []
        for (let i = 0; i < this.clientes.length; i++){
            todosClientes.push(this.clientes[i].nome)
        }
        return todosClientes
    }

    listarDadosDoCliente(cliente){
        const dados = this.clientes.find( e => e.nome === cliente)
        if(dados){
            return dados
        }
        return 'Não existe registros referentes a este Cliente'
    }


}