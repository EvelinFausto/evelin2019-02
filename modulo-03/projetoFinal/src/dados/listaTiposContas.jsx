import TiposContas from "./tiposContas";

export default class ListaTiposContas {
    constructor() {
        this.tiposContas = [
            { tipo: 'Corrente', codigo:'001' },
            { tipo: 'Poupança', codigo:'002' },
            { tipo: 'Estudante', codigo:'003' },
            { tipo: 'Empresarial', codigo:'004' },
            { tipo: 'Salario', codigo:'005' },
        ].map( t => new TiposContas(t.tipo, t.codigo ))
    }
    
    get listarTodoTipoConta() {
        const todosTiposContas = []
        for (let i = 0; i < this.tiposContas.length; i++){
            todosTiposContas.push(this.tiposContas[i].tipo)
        }
        return todosTiposContas
    }

    listarDadosTiposContas(conta){
        const dados = this.tiposContas.find( e => e.tipo === conta)
        if(dados){
            return dados
        }
        return 'Não existe registros referentes a este Cliente'
    }

}