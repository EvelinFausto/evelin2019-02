export default class Cliente {
    constructor( nome, cpf, endereco, telefone, email ) {
        this.nome = nome
        this.cpf = cpf
        this.endereco = endereco
        this.telefone = telefone
        this.email = email
    }

}