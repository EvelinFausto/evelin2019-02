//import React from 'react'
import Agencia from "./agencia";

export default class ListaAgencias {
    constructor() {
        this.agencias = [
            { nome: 'Bs2', endereco:'Av.Assis Brasil, 2000', telefone:'51 3230-3230', codigo:'001' },
            { nome: 'Santander', endereco:'Av.Pernambuco, 320', telefone:'51 3333-3333', codigo:'002' },
            { nome: 'Banco do Brasil', endereco:'Av.Padre cacique, 950', telefone:'51 3111-1111', codigo:'003'},
            { nome: 'Itau', endereco:'Av.7 de Setembro, 7', telefone:'51 3311-2222', codigo:'004' },
            { nome: 'Banrisul', endereco:'Av.Imortal Tricolor, 7', telefone:'51 3145-4545', codigo:'005' },
            { nome: 'Bradesco', endereco:'Av.Inter Sofredor, 10', telefone:'51 3311-7676', codigo:'006' },

        ].map( a => new Agencia(a.nome, a.endereco, a.telefone, a.codigo ))
    }

    get listarTodasAgencias() {
        const agenciasResult = []
        for (let i = 0; i < this.agencias.length; i++){
            agenciasResult.push(this.agencias[i].nome)
        }
        return agenciasResult
    }

    listarDadosDaAgencia(agencia){
        const dados = this.agencias.find( e => e.nome === agencia)
        if(dados){
            return dados
        }
        return 'Não existe registros referentes a esta agência'
    }

}