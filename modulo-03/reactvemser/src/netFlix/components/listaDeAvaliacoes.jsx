import React from 'react';


const ListaDeAvaliacoes = ( { nota, episodio } ) => 

        <React.Fragment>
            <label>Episódio {episodio} recebeu a nota: {nota}</label>
        </React.Fragment>
    
export default ListaDeAvaliacoes