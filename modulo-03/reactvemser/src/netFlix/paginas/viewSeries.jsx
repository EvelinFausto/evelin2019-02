import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import ListaSeries from './viewSeries2';
import paginaInicial from './paginaInicial';
//import Mirror from './ReactMirror';
import Login from './login';
import {PrivateRoute} from './../components/PrivateRoute';
import ListaAvaliacoes from './listaAvaliacoes';

import App from './../../App'


//import Nav from './netFlix/header'


class ViewSeries extends Component {
    render() {
        return (
          <div className="">
            <Router>
              <React.Fragment>
                <section>
                  <PrivateRoute path="/" exacts component={paginaInicial} />
                  <Route path="/login" component={Login} />
                  <Route path="/home" component={ListaSeries } />
                  <Route path="/mirror" component={App} />
                  <Route path="/avaliacoes" component={ListaAvaliacoes} />
                </section>
              </React.Fragment>
            </Router>
          </div>
        );
      }
      return
}

// const testeRota = () => <h2> Testando Rota</h2>


export default ViewSeries;