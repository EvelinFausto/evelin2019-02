import React, { Component } from 'react';
import ListaSeries from './../ListarSeries';
// import {Link } from 'react-router-dom';

import './../css/home.css';



class ViewSeries extends Component {
    constructor ( props ) {
        super( props )
        this.listaSeries = new ListaSeries();
        this.state = {
        verificarSerieInvalida: this.listaSeries.verificarSerieInvalida,
        listarTodos: this.listaSeries.listarTodos,
        seriePorAno: this.listaSeries.filtrarPorAno(2017) ,
        pesquisaNome: this.listaSeries.pesquisaNome('Evelin Fausto'),
        totalSalarios: this.listaSeries.totalSalarios(1),
        mediaEpisodios: this.listaSeries.mediaDeEpisodios,
        pesquisaPeloGenero: this.listaSeries.pesquisaPorGenero("Caos"),
        pesquisaPeloTitulo:this.listaSeries.pesquisaPeloTitulo("The"),
        creditos: this.listaSeries.creditos(1),
        mostrarPalavraSecreta: this.listaSeries.mostrarPalavraSecreta,
        nome: false,
        ano: false,
        bot: false,
        salario: false,
        todas: false,
        media: false,
        seriesInvalidas: false,
        genero:false,
        titulo:false,
        credito:false,
        exibirMensagem: false
        }
    }

    elenco() {
      return(
        <div>
          {
            this.state.nome && (
            <div>
              <span> Elenco: </span>
              <input type="text" placeholder="Ex: Evelin"></input>
              <button onClick="">Pesquisar</button>
              <h2>Elenco :</h2>
              <h3>{ this.state.pesquisaNome }</h3>
            </div>
            )
          }
        </div>
      )
    }

    pAno( evt ) {
      this.props.filtrarPorAno(evt.target.value)
      this.setState({
        exibirMensagem: true
        })
      setTimeout( () => {
        this.setState({
          exibirMensagem:false
        })
      }, 5000)
    }

    ano() {
      return(
        <div>
          {
            this.state.ano && (
              <div className="containerTotal">
                <label>Pesquise séries apartir do ano: </label>
                <input type="number" placeholder="Ex: 2017" onBlur={this.pAno.bind( this)}></input>
                <button onClick="">Pesquisar</button>
                <h2>Séries do ano 2017 :</h2>
                <h3>{ this.exibirMensagem ? 'this.seriesAno' : 'Não á séries neste periodo' }</h3>
              </div>
            )
          }
        </div>
      )
    }

    gastos() {
      return(
        <div>
          {
            this.state.salario && (
              <div>
              <span>Informe o indice: </span>
              <input type="number" placeholder="Ex: 1"></input>
              <button onClick="">Pesquisar</button>
              <h2>Gasto em salário :</h2>
              <h3>{ this.state.totalSalarios }</h3>
            </div>
            )
          }
        </div>
      )
    }

    listarSeries() {
      return(
        <div>
          {
            this.state.todas && (
              <div className="containerTotal text-center float-left">
                <h2>Séries :</h2>
                <h3>{this.state.listarTodos}</h3>
            </div>
            )
          }
        </div>
      )
    }

    mediaDeEpisodios() {
      return(
        <div>
          {
            this.state.media && (
              <div>
                <h1>média = { this.state.mediaEpisodios } episódios </h1>
            </div>
            )
          }
        </div>
      )
    }

    seriesInvalida() {
      return(
        <div>
          {
            this.state.seriesInvalidas && (
              <div className="container float-left text-center">
                <h2>Séries inválidas: </h2>
                <h3>{ this.state.verificarSerieInvalida }</h3>
            </div>
            )
          }
        </div>
      )
    }

    pesquisaGenero() {
      return(
        <div>
          {
            this.state.genero && (
              <div>
                <span>Informe o gênero: </span>
                <input type="text" placeholder="Ex: Caos"></input>
                <button onClick="">Pesquisar</button>
                {/* <h2>{ this.state.pesquisaPeloGenero }</h2> */}
                <h3>Corrigir</h3>
               </div>
            )
          }
        </div>
      )
    }

    pesquisaTitulo() {
      return(
        <div>
          {
            this.state.titulo && (
              <div>
                <span>Informe um título: </span>
                <input type="text" placeholder="Ex: The Walking Dead"></input>
                <button onClick="">Pesquisar</button>
                {/* <h2>{ this.state.pesquisaPeloTitulo }</h2> */}
                <h3>Corrigir</h3>
            </div>
            )
          }
        </div>
      )
    }

    pesquisaCreditos() {
      return(
        <div>
          {
            this.state.credito && (
              <div>
                <span>Informe um indice: </span>
                <input type="number" placeholder="Ex: 1"></input>
                <button onClick="">Pesquisar</button>
                <h2>{ this.state.creditos }</h2>
            </div>
            )
          }
        </div>
      )
    }

    render() {
        return (
          <div className="geral">
            <div className="botoesHome">
            <button onClick={() => this.setState({ todas: true })}>Séries</button>
              <button onClick={() => this.setState({ seriesInvalidas: true })}>Séries inválidas</button>
              <button onClick={() => this.setState({ ano: true })}>Pesquisar pelo ano</button>
              <button onClick={() => this.setState({ nome: true })}>Pesquisar pelo elenco</button>
              <button onClick={() => this.setState({ salario: true })}>Gastos</button>
              <button onClick={() => this.setState({ media: true })}>Média de episódios</button>
              <button onClick={() => this.setState({ genero: true })}>Pesquisar pelo gênero</button>
              <button onClick={() => this.setState({ titulo: true })}>Pesquisar pelo título</button>
              <button onClick={() => this.setState({ credito: true })}>Créditos</button>
            </div>
            <div className="resultados">
              {this.listarSeries()}
              {this.seriesInvalida()}
              {this.ano()}
              {this.elenco()}
              {this.gastos()}
              {this.mediaDeEpisodios()}
              {this.pesquisaGenero()}
              {this.pesquisaTitulo()}
              {this.pesquisaCreditos()}
              {this.state.mostrarPalavraSecreta}
            </div>
          </div>
        );
      }
      return
}

export default ViewSeries;