import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Nav from './../header'

import './../css/inicio.css';


class ViewSeries extends Component {
  botoes(){
    return(
      <div>
        {
          this.state.bot && (
            <div>
            <button onClick={() => this.setState({ ano: true })}>Pesquisar Ano</button>     
            <button onClick={() => this.setState({ campo: true })}>Pesquisar Elenco</button>
          </div>
          )
        }
      </div>
    )
  }

    render() {
        return (
          <div >
            <Router>
              <React.Fragment>
                <section>
                  <Nav mostrarBotoes={ this.botoes.bind( this ) }/>
                </section>
              </React.Fragment>
            </Router>
          </div>
        );
      }
      return
}



export default ViewSeries;