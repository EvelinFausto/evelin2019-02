import React, { Component } from 'react';
import ListaDeAvaliacoes from './../components/listaDeAvaliacoes';

class ListaAvaliacoes extends Component {
    constructor(props){
        super(props)
        this.state = {
            nota: [],
            episodio:[]
        }
    }

    listarEpisodiosAvaliados() {
        this.setState({
            nota:[this.props.nota],
            episodio:[this.props.nome]
        })
        
    }

    render() {
        const {episodio, nota} = this.state;
        return (
            <React.Fragment>
                <ListaDeAvaliacoes nota={nota} episodio={episodio} />
            </React.Fragment>
        );
      }
}



export default ListaAvaliacoes;