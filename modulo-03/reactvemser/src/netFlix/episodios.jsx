export default class Episodios {
    constructor( titulo, anoEstreia, diretor, genero, elenco, temporadas, numeroEpisodios, distribuidora, imagem ) {
        this.titulo = titulo
        this.anoEstreia = anoEstreia
        this.diretor = diretor
        this.genero = genero
        this.elenco = elenco
        this.temporadas = temporadas
        this.numeroEpisodios = numeroEpisodios
        this.distribuidora = distribuidora
        this.imagem = imagem
    }

}