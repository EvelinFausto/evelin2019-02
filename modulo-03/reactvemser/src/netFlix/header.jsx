import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import {Link } from 'react-router-dom';

import './css/header.css';

const Nav = props => {
    return (
        <Router>
            <React.Fragment>
		    <div className="nav">
                <div className="row">
                    <div className="logo pull-left">
                        <label>Pirate</label>
                    </div>
                    <div className="pull-right">
                        <button className="Button" onClick={props.mostrarBotoes}>Pesquisar</button>
                    </div>
                    <ul className="text-center">
                        <li><Link to="../">Inicio</Link></li>
                        <li><Link to="../home">Home</Link></li>
                        <li><Link to="../mirror">Mirror</Link></li>    
                        <li><Link to="../avaliacoes">Avaliações</Link></li>                      
                        <button type="button" onClick={ props.logout}>Deslogar</button>

                    </ul>
                </div>
            </div>
        </React.Fragment>
        </Router>
    )
}

export default Nav