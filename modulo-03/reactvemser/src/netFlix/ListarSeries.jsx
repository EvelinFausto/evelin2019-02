import Episodios from "./episodios";
import PropTypes from 'prop-types';

export default class ListaSeries {
    constructor() {
        this.series = [
        {titulo:"Stranger Things", anoEstreia:2016, diretor:["Matt Duffer", "Ross Duffer"], genero:["Suspense", "Ficcao Cientifica", "Drama"], elenco:["Winona Ryder","c Harbour","Finn Wolfhard","Millie Bobby Brown","Gaten Matarazzo","Caleb McLaughlin","Natalia Dyer","Charlie Heaton","Cara Buono","Matthew Modine","Noah Schnapp"], temporadas:2, numeroEpisodios:17, distribuidora:"Netflix", imagem:"img/the-walkiing-dead.jpeg"},
        {titulo:"Game Of Thrones", anoEstreia:2011 ,diretor:["David Benioff","D. B. Weiss","Carolyn Strauss","Frank Doelger","Bernadette Caulfield","George R. R. Martin"],genero:["Fantasia","Drama"], elenco:["Peter Dinklage","Nikolaj Coster-Waldau","Lena Headey","Emilia Clarke","Kit Harington","Aidan Gillen","Iain Glen ","Sophie Turner","Maisie Williams","Alfie Allen","Isaac Hempstead Wright"],temporadas:7,numeroEpisodios:67,distribuidora:"HBO", imagem:"img/the-walkiing-dead.jpeg"},
        {titulo:"The Walking Dead", anoEstreia:2010,diretor:["Jolly Dale","Caleb Womble","Paul Gadd","Heather Bellson"],genero:["Terror","Suspense","Apocalipse Zumbi"],elenco:["Andrew Lincoln","Jon Bernthal","Sarah Wayne Callies","Laurie Holden","Jeffrey DeMunn","Steven Yeun","Chandler Riggs ","Norman Reedus","Lauren Cohan","Danai Gurira","Michael Rooker ","David Morrissey"],temporadas:9,numeroEpisodios:122,distribuidora:"AMC", imagem:"img/the-walkiing-dead.jpeg"},
        {titulo:"Band of Brothers", anoEstreia:20001,diretor:["Steven Spielberg","Tom Hanks","Preston Smith","Erik Jendresen","Stephen E. Ambrose"],genero:["Guerra"],elenco:["Damian Lewis","Donnie Wahlberg","Ron Livingston","Matthew Settle","Neal McDonough"],temporadas:1,numeroEpisodios:10,distribuidora:"HBO", imagem:"img/the-walkiing-dead.jpeg"},
        {titulo:"The JS Mirror",anoEstreia:2017,diretor:["Lisandro","Jaime","Edgar"],genero:["Terror","Caos","JavaScript"],elenco:["Daniela Amaral da Rosa","Antônio Affonso Vidal Pereira da Rosa","Gustavo Lodi Vidaletti","Bruno Artêmio Johann Dos Santos","Márlon Silva da Silva","Izabella Balconi de Moura","Diovane Mendes Mattos","Luciano Maciel Figueiró","Igor Ceriotti Zilio","Alexandra Peres","Vitor Emanuel da Silva Rodrigues","Raphael Luiz Lacerda","Guilherme Flores Borges","Ronaldo José Guastalli","Vinícius Marques Pulgatti"],temporadas:1,numeroEpisodios:40,distribuidora:"DBC", imagem:"img/the-walkiing-dead.jpeg"},
        {titulo:"10 Days Why",anoEstreia:2010,diretor:["Brendan Eich"],genero:["Caos","JavaScript"],elenco:["Brendan Eich","Bernardo Bosak"],temporadas:10,numeroEpisodios:10,distribuidora:"JS", imagem:"img/the-walkiing-dead.jpeg"},
        {titulo:"Mr. Robot",anoEstreia:2018,diretor:["Sam Esmail"],genero:["Drama","Techno Thriller","Psychological Thriller"],elenco:["Rami Malek","Carly Chaikin","Portia Doubleday","Martin Wallström","Christian Slater"],temporadas:3,numeroEpisodios:32,distribuidora:"USA Network", imagem:"img/the-walkiing-dead.jpeg"},
        {titulo:"Narcos",anoEstreia:2015,diretor:["Paul Eckstein","Mariano Carranco","Tim King","Lorenzo O Brien"],genero:["Documentario","Crime","Drama"],elenco:["Wagner Moura","Boyd Holbrook","Pedro Pascal","Joann Christie","Mauricie Compte","André Mattos","Roberto Urbina","Diego Cataño","Jorge A. Jiménez","Paulina Gaitán","Paulina Garcia"],temporadas:3,numeroEpisodios:30,distribuidora:null, imagem:"img/the-walkiing-dead.jpeg"},
        {titulo:"Westworld",anoEstreia:2016,diretor:["Athena Wickham"],genero:["Ficcao Cientifica","Drama","Thriller","Acao","Aventura","Faroeste"],elenco:["Anthony I. Hopkins","Thandie N. Newton","Jeffrey S. Wright","James T. Marsden","Ben I. Barnes","Ingrid N. Bolso Berdal","Clifton T. Collins Jr.","Luke O. Hemsworth"],temporadas:2,numeroEpisodios:20,distribuidora:"HBO", imagem:"img/the-walkiing-dead.jpeg"},
        {titulo:"Breaking Bad",anoEstreia:2008,diretor:["Vince Gilligan","Michelle MacLaren","Adam Bernstein","Colin Bucksey","Michael Slovis","Peter Gould"],genero:["Acao","Suspense","Drama","Crime","Humor Negro"],elenco:["Bryan Cranston","Anna Gunn","Aaron Paul","Dean Norris","Betsy Brandt","RJ Mitte"],temporadas:5,numeroEpisodios:62,distribuidora:"AMC", imagem:"img/the-walkiing-dead.jpeg"}
    ].map( e => new Episodios( e.titulo, e.anoEstreia, e.diretor, e.genero, e.elenco, e.temporadas, e.numeroEpisodios, e.distribuidora, e.imagem))
    }
    
    get listarTodos() {
        const nomeSeries = []
        for (let i = 0; i < this.series.length; i++){
            nomeSeries.push(`${this.series[i].titulo} | `)
        }
        return nomeSeries

    }

     get verificarSerieInvalida() {
        let seriesInvalidas = []
        for (let i = 0; i < this.series.length; i++){
            if ((this.series[i].anoEstreia > 2019) || !isNaN(this.series[i])){
                seriesInvalidas.push(this.series[i].titulo)
            }
        }   
        return seriesInvalidas
    }

    filtrarPorAno(ano) {
        let seriesAno = []
        for (let i = 0; i < this.series.length; i++){
            if (this.series[i].anoEstreia >= ano){
                seriesAno.push(`${this.series[i].titulo} | `)
            }
        
        } 
        return this.seriesAno
    }

    pesquisaNome (nome) {
        let encontrou = false;
        this.series.forEach( ( serie ) => {
            let achou = serie.elenco.find( n => nome === n )
            if ( achou ) {
            encontrou = true
            }
        } )
        return encontrou
    }    

    get mediaDeEpisodios() {
        return parseFloat( this.series.map( s => s.numeroEpisodios ).reduce( ( acc, elem ) => acc+ elem, 0 ) / this.series.length )
    }

    totalSalarios( indice ) {
        function imprimirBRLOneLine( valor ) {
            return parseFloat( valor.toFixed( 2 ) ).toLocaleString( 'pt-BR', {
            style: 'currency',
            currency: 'BRL'
            } )
        }
        const salarioDirecao = 100000 * this.series[ indice ].diretor.length
        const salarioAtores = 40000 * this.series[ indice ].elenco.length
        return imprimirBRLOneLine( parseFloat( salarioAtores + salarioDirecao ) )
    }

    pesquisaPorGenero( genero ) {
        return this.series.filter( ( serie ) => {
            return Boolean( serie.genero.find( ( g ) => g === genero ) )
        } )
    }

    pesquisaPeloTitulo( titulo ) {
        let palavrasTitulo = titulo.split( ' ' )
        return this.series.filter( ( serie ) => {
          let palavrasSerie = serie.titulo.split( ' ' )
          return Boolean( palavrasSerie.find( ( palavra ) => palavra.includes( palavrasTitulo ) ) )
        } )
    }

    creditos( indice ) {
        this.series[ indice ].elenco.sort( (a, b) => {
            let arrayA = a.split( ' ' )
            let arrayB = b.split( ' ' )
            return arrayA[ arrayA.length - 1 ].localeCompare( arrayB[ arrayB.length - 1 ] )
        } )
        this.series[ indice ].diretor.sort( (a, b) => {
            let arrayA = a.split( ' ' )
            let arrayB = b.split( ' ' )
            return arrayA[ arrayA.length - 1 ].localeCompare( arrayB[ arrayB.length - 1 ] )
        } )
        let arrayParaInner = []
        
        arrayParaInner.push( `${this.series[indice].titulo}` )
        arrayParaInner.push( 'Diretores' )
        
        this.series[ indice ].diretor.forEach( ( d ) => {
            arrayParaInner.push( `${d}` )
        } )
        arrayParaInner.push( 'Elenco' )
        
        this.series[ indice ].elenco.forEach( ( d ) => {
            arrayParaInner.push( `${d}` )
        } )

        return arrayParaInner;
    }

    matchesNomeAbreviado(s) {
        let verifica = true //s.match( '\\s+[a-zA-Z]{1}\.\\s+' )
        return ( verifica )? Boolean(verifica[0]) : undefined
      }
        
      letraAbreviada(s) {
        if(this.matchesNomeAbreviado(s)) 
        return 'corrigir' //s.match( '\\s+[a-zA-Z]{1}\.\\s+' )[0].charAt(1)
      }
        
      serieComTodosNomesAbreviados() {
        return this.series.filter( (serie) => {
          return !serie.elenco.find( s => !this.matchesNomeAbreviado(s) )
        } )
      }
        
      mostrarPalavraSecreta() {
        let palavra = "#"
        let array = this.serieComTodosNomesAbreviados()
        array[0].elenco.forEach( (s) => {
          palavra = palavra.concat(this.letraAbreviada(s))
        })
        return palavra
    }
}

ListaSeries.propTypes = {

    listarTodos:PropTypes.array,
    verificarSerieInvalida: PropTypes.array,
    filtrarPorAno: PropTypes.array,
    pesquisaNome: PropTypes.array,
    mediaDeEpisodios: PropTypes.array,
    totalSalarios: PropTypes.array,
    pesquisaPorGenero: PropTypes.array,
    pesquisaPeloTitulo: PropTypes.array,
    creditos: PropTypes.array,    
    matchesNomeAbreviado: PropTypes.string,
    letraAbreviada: PropTypes.string,
    serieComTodosNomesAbreviados: PropTypes.array,
    mostrarPalavraSecreta: PropTypes.array,
};
