import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './models/ListaEpisodios'
import EpisodioPadrao from './components/episodioPadrao'
import * as Axios from 'axios';

//console.log(ListaEpisodios)

class App extends Component {
  constructor ( props ) {
    super( props )
    this.listaEpisodios = new ListaEpisodios();
    //this.sortear = this.sortear.bind( this )
    this.state = {
      episodio: this.listaEpisodios.episodiosleatorios,
      exibirMensagem:false,
      nota: '',
      nomeEpisodio: '',
      c: false
    }
  }

  componentDidMount() {
    Axios.get("https://pokeapi.co/api/v2/pokemon/").then( response => console.log(response))
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodiosleatorios
    this.setState({
      episodio
    })
  }

  marcarComoAssistido = () => {
    const {episodio} = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState({
      episodio
    })
  }

  registrarNota( evt ) {
    const { episodio } = this.state
    episodio.avaliar(evt.target.value)
    this.setState({
      episodio,
      exibirMensagem: true
    })
    setTimeout( () => {
      this.setState({
        exibirMensagem:false
      })
    }, 5000)
  }

  gerarCampoDeNota() {
    return(
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <span> Qual a sua nota para este episódio ? </span>
              <input type="number" placeholder="1 a 5" onBlur={this.registrarNota.bind( this)}></input>
            </div>
          )
        }
      </div>
    )
  }

  episodiosAssistidosComNota() {
    if ( (this.state.episodio.assistido) && (this.state.episodio.nota ) ){
      this.setState({
        nota: "7",
        nomeEpisodio: "nome",
        c: true
      })
    }
  }


  logout() {
    localStorage.removeItem('Authorization');
  }

  render() {
    const { episodio , exibirMensagem } = this.state
    return (
      <div className="App">
        <div className="app-Header">
        <button type="button" onClick={ this.logout.bind(this)}>Deslogar</button>
          <EpisodioPadrao episodio={ episodio } sortearNoComp={ this.sortear.bind( this ) } marcarNoComp={ this.marcarComoAssistido.bind( this ) } />
          {this.gerarCampoDeNota()}
          <h5>{ exibirMensagem ? 'Nota registrada com sucesso!' : ''}</h5>
          {this.episodiosAssistidosComNota()? 'sss': 'ddd'}
          {this.state.c ? 'ok' : 'não'}
        </div>
      </div>
    );
  }
  return
}

export default App;
