import React from 'react';

const EpisodioPadrao = props => {
    const { episodio } = props
    return (
        <React.Fragment>
			<div className="boton">
				<button onClick={ props.sortearNoComp }>Próximo</button>
          	    <button onClick={ props.marcarNoComp }>Já Assisti</button>
			</div>
          	<h2>{ episodio.nome } </h2>
            <img src={episodio.thumbUrl} alt={episodio.nome}></img>
            <h4> Temporada / Episódio: { episodio.temporadaEpisodio } Duração de: { episodio.duracaoEmMin }</h4>
            <h4>Já assisti? { episodio.assistido ? 'Sim' : 'Não' }, { episodio.qtdVezesAssistido > 1 ? `${episodio.qtdVezesAssistido} vezes` : `${episodio.qtdVezesAssistido} vez` }</h4>
            <h4>Nota : {episodio.nota || 'Não consta'}</h4>
        </React.Fragment>
    )
}

export default EpisodioPadrao