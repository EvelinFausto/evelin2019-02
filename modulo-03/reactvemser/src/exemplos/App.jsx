import React, { Component } from 'react';
import './App.css';
import ListaTime from './models/ListaTime'
// import Filho from './exemplos/Filhos'
//import CompA, { CompB } from './ExemploComponenteBasico';
import Familia from './exemplos/Familia'

console.log(ListaTime)

class App extends Component {
  constructor ( props ) {
    super( props )
    this.ListaTime = new ListaTime()
    console.log(this.listaTime)
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          {/* <Filho titulo = { 'Nome' }/> */}
          <Familia nome={ 'Antonio'} sobrenome={ 'Pereira' }/>
          <Familia nome={ 'Pedro'} sobrenome={ 'Pereira' }/>
          <Familia nome={ 'Maria'} sobrenome={ 'Pereira' }/>
          <Familia nome={ 'Carlos'} sobrenome={ 'Pereira' }/>
          <ul>
            { this.ListaTime.todos.map((time) => {
              return `Time: ${ time.nome } | Estado: ${ time.estado } `
            }) }
          </ul>
        </header>
      </div>
    );
  }
  return
}

export default App;
