import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest{
    private final double DELTA = 1e-9;
    
    @Test
    public void ElfoDevePerderVida(){
        ElfoDaLuz feanor = new ElfoDaLuz("feanor ");
        Dwarf gul = new Dwarf("gul");
        feanor.atacarComEspada(gul);
        assertEquals(79.0,feanor.getVida(), DELTA);
        assertEquals(100.0,gul.getVida(), DELTA);
    }
    
    @Test
    public void ElfoDevePerderVidaEGanhar(){
        ElfoDaLuz feanor = new ElfoDaLuz("feanor ");
        Dwarf gul = new Dwarf("gul");
        feanor.atacarComEspada(gul);
        feanor.atacarComEspada(gul);
        assertEquals(89.0,feanor.getVida(), DELTA);
        assertEquals(90.0,gul.getVida(), DELTA);
    }
}
