import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest{
    private final double DELTA = 1e-9;
    
    @Test
    public void ElfonoturnoGana3XpaoAtirarFlecha(){
        ElfoNoturno novoElfoNoturno = new ElfoNoturno("escuridão");
        novoElfoNoturno.atirarFlechaDwarf(new Dwarf("anao"));
        assertEquals(3, novoElfoNoturno.getExperiencia());
    }
    
    @Test
    public void Elfonoturnoperde15Vida(){
        ElfoNoturno novoElfoNoturno = new ElfoNoturno("escuridão");
        novoElfoNoturno.atirarFlechaDwarf(new Dwarf("anao"));
        assertEquals(85.0,novoElfoNoturno.getVida(), DELTA);
        assertEquals(Status.SOFREU_DANO,novoElfoNoturno.getStatus());
    }
    
    @Test
    public void ElfonoturnoAtira7vezesEMorre(){
        ElfoNoturno novoElfoNoturno = new ElfoNoturno("escuridão");
        novoElfoNoturno.getFlecha().setQuantidade(1000);
        novoElfoNoturno.atirarFlechaDwarf(new Dwarf("anao"));
        novoElfoNoturno.atirarFlechaDwarf(new Dwarf("anao"));
        novoElfoNoturno.atirarFlechaDwarf(new Dwarf("anao"));
        novoElfoNoturno.atirarFlechaDwarf(new Dwarf("anao"));
        novoElfoNoturno.atirarFlechaDwarf(new Dwarf("anao"));
        novoElfoNoturno.atirarFlechaDwarf(new Dwarf("anao"));
        novoElfoNoturno.atirarFlechaDwarf(new Dwarf("anao"));
        assertEquals(0.0,novoElfoNoturno.getVida(), DELTA);
        assertEquals(Status.MORTO,novoElfoNoturno.getStatus());
    }
    
   @Test
    public void verificaSeElfoNasceComArcoEFlecha(){
        ElfoNoturno novoElfoNoturno = new ElfoNoturno("escuridão");
        assertEquals(2,novoElfoNoturno.getQtdFlecha());
        assertEquals("Arco,flecha",novoElfoNoturno.getInventario().getDescricoesItens());
    }
   
    @Test
    public void verificaSeatirarFlechaDiminuir15DeVidaEGanha3Experiencia(){
        Elfo novoElfoNoturno = new ElfoNoturno("escuridão");
        Dwarf novoDwarf = new Dwarf("anao");
        novoElfoNoturno.atirarFlechaDwarf(novoDwarf);
        assertEquals(3,novoElfoNoturno.getExperiencia());
        assertEquals(1,novoElfoNoturno.getQtdFlecha());
        assertEquals(85.0,novoElfoNoturno.getVida(), DELTA);
    }
    
    
}
