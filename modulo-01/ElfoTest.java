import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest{
    
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void atirarFlechaDwarfDiminuirFlexaAumentarXP(){
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoDwarf = new Dwarf("anao");
        novoElfo.atirarFlechaDwarf(novoDwarf);
        assertEquals(1,novoElfo.getExperiencia());
        assertEquals(1,novoElfo.getQtdFlecha());
    }
    
    @Test
    public void atirarFlecha3VezesDeveDiminuirFlexaAumentarXP(){
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoDwarf = new Dwarf("anao");
        novoElfo.atirarFlechaDwarf(novoDwarf);
        novoElfo.atirarFlechaDwarf(novoDwarf);
        novoElfo.atirarFlechaDwarf(novoDwarf);
        assertEquals(2,novoElfo.getExperiencia());
        assertEquals(0,novoElfo.getQtdFlecha());
    }
    
    @Test
    public void elfosNascemCom2Flechas(){
        Elfo novoElfo = new Elfo ("legolas");
        assertEquals(2,novoElfo.getQtdFlecha());
    }
    
    @Test
    public void elfosNascemComStatusRecemCriado(){
        Elfo novoElfo = new Elfo ("legolas");
        assertEquals(Status.RECEM_CRIADO,novoElfo.getStatus());
    }
    
    @Test 
    public void criarUmElfoIncrementaCondutorUmaVez(){
        new Elfo("Legolas");
        assertEquals(1, Elfo.getQtdElfos());
    }
    
    @Test 
    public void criarDoisElfoIncrementaCondutor2Vezes(){
        new Elfo("Legolas");
        new Elfo("Legolas");
        assertEquals(2, Elfo.getQtdElfos());
    }
    
    @Test 
    public void naoCriarElfoIncrementaCondutor(){
        assertEquals(0, Elfo.getQtdElfos());
    }
    
}
