import java.util.*;

public class Inventario{
    private ArrayList<Item> items;
    
    public Inventario(int quantidade){
        this.items = new ArrayList<>(quantidade);
    }
    
    public Item obter(){
        return null;
    }
    
    public ArrayList<Item> getItens(){
        return this.items;
    }
    
    public Item obter(int posicao){
        if (posicao >= this.items.size()){
            return null;
        }
        return this.items.get(posicao);
    }
    
    public void remover(Item item){
        this.items.remove(item);
    }
    
    public void adicionar(Item item){
        this.items.add(item);
    }
    
    public String getDescricoesItens(){
        StringBuilder descricoes = new StringBuilder();
        for(int i = 0; i < items.size(); i++){
            Item item = this.items.get(i);
            if(items != null){
                descricoes.append(item.getDescricao());
                descricoes.append(",");
            }
        }
        
        return descricoes.length() > 0 ? descricoes.substring(0,(descricoes.length()-1)) : descricoes.toString();
    }
    
    public Item getItemComMaiorQuantidade(){
        int indice = 0, maiorQuantidade = 0;
        for(int i = 0; i < this.items.size(); i++){
            Item item = this.items.get(i);
            if(items != null){
                if(item.getQuantidade() > maiorQuantidade){
                    maiorQuantidade = item.getQuantidade();
                    indice = i;
                }
            }
        }
        
        return this.items.size() > 0 ? this.items.get(indice) : null;
    }
    
    public Item Buscar(String descricao){
        for ( Item itemAtual : this.items){
            boolean encontrei = itemAtual.getDescricao().equals(descricao);
            if (encontrei){
                return itemAtual;
            }
        }
        return null;
    }
   
    
    public boolean verificaEscudo(){
        for ( Item itemAtual : this.items){
            boolean encontrei = itemAtual.getDescricao().equals("Escudo");
            if (encontrei){
                return true;
            }
        }
        return false;
    }
    
    public ArrayList<Item> inverter(){
        ArrayList<Item> listaInvertida = new ArrayList<>(this.items.size());
        
        for ( int i = this.items.size() -1; i>= 0; i--){
            listaInvertida.add(this.items.get(i));
        }
        
        return listaInvertida;
    }
    
    public void ordenarItens(){
        this.ordenarItens(TipoOrdenacao.ASC);
    }
        
    public void ordenarItens(TipoOrdenacao ordenacao){
        for (int i = 0; i < this.items.size(); i++){
            for (int j = 0; j < this.items.size() -1; j++){
                Item atual = this.items.get(j);
                Item proximo = this.items.get(j);
                boolean deveTrocar = ordenacao == TipoOrdenacao.ASC ? atual.getQuantidade() > proximo.getQuantidade() : atual.getQuantidade() < proximo.getQuantidade();
                if(deveTrocar){
                    Item itemTrocado = atual;
                    this.items.set(j, proximo);
                    this.items.set(j + 1, itemTrocado);
                }
            }
        }
    }
}
