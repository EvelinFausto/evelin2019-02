public class Dwarf extends Personagem{
    private boolean equipado = false;
    
    public Dwarf(String nome){
        super(nome);
        this.vida = 110.0;
        this.qtdDano = 10.0;
        this.inventario.adicionar(new Item(1,"Escudo"));
    }
    
    public Status dwarfComStatusMorto(){
        if(this.vida <= 0.0){
            this.status = Status.MORTO;
        }
        return this.status;
    }
    
    public double calcularDano(){
        return this.equipado ? 5.0 : this.qtdDano;
    }
    
    public void diminuirVida(){
        if(this.podeSofrerDano()){
            //comparacao ? verdadeiro : falso;
            this.vida = this.vida >= this.calcularDano() ? this.vida - this.calcularDano() : 0.0;
        
            if (this.vida == 0.0){
                this.status = Status.MORTO;
            }else{
                this.status = Status.SOFREU_DANO;
            }
        }
    }
    
    public void equiparEscudo(){     
        equipado = true;
    }  
    
    public String imprimirResultado(){
        return"Dwarf";
    }
}
