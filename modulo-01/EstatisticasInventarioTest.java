import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest{
    
    @Test
    public void calcularMediaInventarioVazio(){
        Inventario inventario = new Inventario(1);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertTrue(Double.isNaN(estatisticas.calcularMedia()));
    }
    
    @Test
    public void calcularMediaComUmItem(){
        Inventario inventario = new Inventario(1);
        inventario.adicionar( new Item(2, "Escudo"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(2,estatisticas.calcularMedia(), 1e-9);
    }
    
    @Test
    public void calcularMediaComDoisItens(){
        Inventario inventario = new Inventario(1);
        inventario.adicionar( new Item(2, "Escudo"));
        inventario.adicionar( new Item(2, "Espada"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(2,estatisticas.calcularMedia(), 1e-9);
    }
    
    @Test
    public void calcularMedianaInventarioVazio(){
        Inventario inventario = new Inventario(1);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertTrue(Double.isNaN(estatisticas.calcularMediana()));
    }
    
    @Test
    public void calcularMediana(){
        Inventario inventario = new Inventario(3);
        inventario.adicionar( new Item(1, "Escudo"));
        inventario.adicionar( new Item(3, "Espada"));
        inventario.adicionar( new Item(4, "Lança"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(3,estatisticas.calcularMediana(), 1e-9);
    }
    
    @Test
    public void calcularMedianaComDoisItensImpar(){
        Inventario inventario = new Inventario(2);
        inventario.adicionar( new Item(7, "Escudo"));
        inventario.adicionar( new Item(2, "Espada"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(4.5,estatisticas.calcularMediana(), 1e-9);
    }
}
