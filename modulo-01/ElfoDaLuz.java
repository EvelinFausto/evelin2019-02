import java.util.*;

public class ElfoDaLuz extends Elfo{
    private int qtdAtaques;
    private double QTD_VIDA_GANHA = 10.0;
    
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(
        Arrays.asList(
        "Espada de Galvorn"
        )
    );
    
    
    public ElfoDaLuz(String nome){
        super(nome);
        qtdDano = 21;
        this.qtdExperienciaPorAtaque = 1;
        super.ganharItem( new ItemSempreExistente(1, DESCRICOES_OBRIGATORIAS.get(0)));
    }
    
    public Item getEspada(){
        return this.getInventario().Buscar(DESCRICOES_OBRIGATORIAS.get(0));
    }
   
    public void perderItem(Item item){
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if(possoPerder){
            super.perderItem(item);
        }
    }
    
    private boolean devePerderVida(){
        return qtdAtaques % 2 == 1;
    }
    
    private void ganharVida(){
        vida += QTD_VIDA_GANHA;
    }
    
    public void atacarComEspada(Dwarf dwarf){
        if(getEspada().getQuantidade() > 0 ){
            qtdAtaques++;
            dwarf.diminuirVida();
            this.aumentarXp();
            if(devePerderVida()){
                diminuirVida();
            }else{
                ganharVida();
            }
        }
    }
}

    // public void atacarComEspada(Dwarf dwarf){
        // this.qtdAtaques = this.qtdAtaques + 1;
        // if(this.qtdAtaques % 2 == 0){
            // this.qtdDano = 0;
            // this.vida = this.vida + 10.0;
        // }else{
            // this.qtdDano = 21.0;
            // diminuirVida();
        // }
        // this.aumentarXp();
        // dwarf.diminuirVida();
    // }
