import java.util.*;

public interface Estrategias{
    public ArrayList<Elfo> noturnosPorUltimo(ArrayList<Elfo> atacantes);
    
    public ArrayList<Elfo> ataqueIntercalado(ArrayList<Elfo> atacantes);
}