import java.util.*;

public class Elfo extends Personagem {
    private int indiceFlecha;
    private static int qtdElfos;
    private OrdenarExercito ordenador;
    {
        indiceFlecha = 1;
        this.inventario = new Inventario(2);
    }
       
    public Elfo(String nome){
        super(nome);
        this.vida = 100.0;
        this.inventario.adicionar(new Item(1,"Arco"));
        this.inventario.adicionar(new Item(2,"flecha"));
        Elfo.qtdElfos++;
    }
    
    protected void finalize() throws Throwable{
        Elfo.qtdElfos--;
    }
    
    public static int getQtdElfos(){
        return Elfo.qtdElfos;
    }
    
    public Item getFlecha(){
        return this.inventario.obter(indiceFlecha);
    }
    
    public int getQtdFlecha(){
        return this.getFlecha().getQuantidade();
    }
   
    public boolean podeAtirarFlecha(){
        return getFlecha().getQuantidade() > 0;
    }
    
    public void atirarFlechaDwarf(Dwarf dwarf){
        int qtdAtual = getFlecha().getQuantidade();
        if (podeAtirarFlecha()){
            this.getFlecha().setQuantidade(qtdAtual - 1);
            this.aumentarXp();
            dwarf.diminuirVida();
            diminuirVida();
        }
    }
    
    public String imprimirResultado(){
        return"Elfo";
    }
    
    public void noturnosAtras(ArrayList<Elfo> elfos, OrdenarExercito ordenador){
        ordenador.noturnosPorUltimo(elfos);
    }
    
    public void intercalados(ArrayList<Elfo> elfos, OrdenarExercito ordenador){
        ordenador.ataqueIntercalado(elfos);
    }
    
    public void EcolhaDeAtaque(String opcao){
       
    }
}