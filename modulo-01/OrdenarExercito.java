import java.util.*;

public class OrdenarExercito extends ExercitoElfos implements Estrategias{

    private ExercitoElfos elfo;
    private final ArrayList<Status> STATUS_PERMITIDO = new ArrayList<>(
        Arrays.asList(
        Status.RECEM_CRIADO, 
        Status.SOFREU_DANO
        )
    );
    private ArrayList<Elfo> elfosVivos = new ArrayList<>();
    private HashMap<Class, ArrayList<Elfo>> ordenarAtaque = new HashMap<>();

    public void verificaElfo(Elfo elfoAlistado){
        boolean podeParticipar = elfoAlistado.getStatus().equals(STATUS_PERMITIDO);
        if(podeParticipar){
            elfosVivos.add(elfoAlistado);
            ArrayList<Elfo> tipoElfo = ordenarAtaque.get(elfoAlistado.getClass());
            if(tipoElfo == null){
                tipoElfo = new ArrayList<>();
                ordenarAtaque.put(elfoAlistado.getClass(), tipoElfo);
            }
            tipoElfo.add(elfoAlistado);
        }
    }
    
    public ArrayList<Elfo> noturnosPorUltimo(ArrayList<Elfo> atacantes){
        ArrayList<Elfo> elfosVerde = new ArrayList<>();
        ArrayList<Elfo> elfosNoturno = new ArrayList<>();
        
        for(Elfo elfo : atacantes){
            if (elfo instanceof ElfoVerde){
                elfosVerde.add(elfo);
            }else if (elfo instanceof ElfoNoturno) {
                elfosNoturno.add(elfo);
            }
        }
        
        elfosVerde.addAll(elfosNoturno);
        return elfosVerde;
    }
    
    public ArrayList<Elfo> ataqueIntercalado(ArrayList<Elfo> atacantes){
        ArrayList<Elfo> lista = noturnosPorUltimo(atacantes);
        ArrayList<Elfo> intercalados = new ArrayList<>();
        int tamanhoLista = lista.size() / 2;
        
        for (int i = 0; i < tamanhoLista; i++){
            intercalados.add(lista.get(i));
            intercalados.add(lista.get(lista.size()-1));
        }
        return intercalados;
    }
    
}