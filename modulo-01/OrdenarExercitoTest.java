import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class OrdenarExercitoTest{
    
    @Test
    public void testeEstrategiaUm(){
        Elfo verde = new ElfoVerde("verde");
        Elfo noturno = new ElfoNoturno("noturno");
        Elfo verde1 = new ElfoVerde("verde");
        Elfo noturno1 = new ElfoNoturno("noturno");
        ArrayList<Elfo> elfos = new ArrayList<>();
        elfos.add(noturno);
        elfos.add(verde);
        elfos.add(noturno1);
        elfos.add(verde1);
        
        ArrayList<Elfo> elfos1 = new ArrayList<>();
        OrdenarExercito exercito = new OrdenarExercito();
        elfos1 = exercito.noturnosPorUltimo(elfos);
        
        assertEquals(ElfoVerde.class, elfos1.get(0).getClass());
        assertEquals(ElfoVerde.class, elfos1.get(1).getClass());
        assertEquals(ElfoNoturno.class, elfos1.get(2).getClass());   
        assertEquals(ElfoNoturno.class, elfos1.get(3).getClass()); 

    }
    
    @Test
    public void testeEstrategiaDoisIntercalandoElfos(){
        Elfo verde = new ElfoVerde("verde");
        Elfo noturno = new ElfoNoturno("noturno");
        Elfo verde1 = new ElfoVerde("verde");
        Elfo noturno1 = new ElfoNoturno("noturno");
        ArrayList<Elfo> elfos = new ArrayList<>();
        elfos.add(noturno);
        elfos.add(noturno1);
        elfos.add(verde);
        elfos.add(verde1);
        
        ArrayList<Elfo> elfos1 = new ArrayList<>();
        OrdenarExercito exercito = new OrdenarExercito();
        elfos1 = exercito.ataqueIntercalado(elfos);
        
        assertEquals(ElfoVerde.class, elfos1.get(0).getClass());
        assertEquals(ElfoNoturno.class, elfos1.get(1).getClass()); 
        assertEquals(ElfoVerde.class, elfos1.get(2).getClass());
        assertEquals(ElfoNoturno.class, elfos1.get(3).getClass()); 

    }
}
