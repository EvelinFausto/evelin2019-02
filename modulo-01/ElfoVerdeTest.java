import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest{
    private final double DELTA = 1e-9;
    
    @Test
    public void elfoVerdeGanha2XpPorUmaFlcha(){
        //Elfo elfo = new ElfoVerde("Celebron");
        ElfoVerde celebron = new ElfoVerde("Celebron");
        celebron.atirarFlechaDwarf(new Dwarf("Balin"));
        assertEquals(2, celebron.getExperiencia());
        
    }
    
    @Test
    public void elfoVerdeAdicionaItemComDescricaoValida(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arcoVidro = new Item(1,"Arco de vidro");
        celebron.ganharItem(arcoVidro);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(1, "Arco"), inventario.obter(0));
        assertEquals(new Item(2, "flecha"), inventario.obter(1));
        assertEquals(arcoVidro, inventario.obter(2));
    }
    
    @Test
    public void elfoVerdeAdicionaItemComDescricaoInvalida(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arcoMadeira = new Item(1,"Arco de madeira");
        celebron.ganharItem(arcoMadeira);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(1, "Arco"), inventario.obter(0));
        assertEquals(new Item(2, "flecha"), inventario.obter(1));
        assertNull(inventario.Buscar("Arco de madeira"));
    }
    
    @Test
    public void elfoVerdePerdeItemComDescricaoValida(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arcoVidro = new Item(1,"Arco de vidro");
        celebron.ganharItem(arcoVidro);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(1, "Arco"), inventario.obter(0));
        assertEquals(new Item(2, "flecha"), inventario.obter(1));
        assertEquals(arcoVidro, inventario.obter(2));
    }
    
    @Test
    public void elfoVerdePerderItemComDescricaoInvalida(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arcoTeste = new Item(1,"Arco Teste");
        celebron.perderItem(arcoTeste);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(1, "Arco"), inventario.obter(0));
        assertEquals(new Item(2, "flecha"), inventario.obter(1));
    }
    
    @Test
    public void verificaSeElfoNasceComArcoEFlecha(){
        ElfoVerde novoElfoVerde = new ElfoVerde("verdão");
        assertEquals(2,novoElfoVerde.getQtdFlecha());
        assertEquals("Arco,flecha",novoElfoVerde.getInventario().getDescricoesItens());
    }   
    
} 
