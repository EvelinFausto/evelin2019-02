public class ElfoNoturno extends Elfo implements ContratoDeAtaque{
    
    OrdenarExercito estrategia;
    
    public ElfoNoturno(String nome){
        super(nome);
        this.qtdExperienciaPorAtaque = 3;
        this.qtdDano = 15.0;
    }
    
    public void EscolhaDeAtaque(OrdenarExercito estrategia){
        this.estrategia = estrategia;
    }
}
