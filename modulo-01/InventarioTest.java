import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class InventarioTest{
    
    @Test
    public void criarInventarioComQuantidadeInformada(){
        Inventario inventario = new Inventario(34);
        for(int i = 0; i < 34; i++){
            Item espada = new Item(i,"espada");
            inventario.adicionar(espada);
        }
        assertEquals(34,inventario.getItens().size());
    }
    
    @Test
    public void adicionarUmItem(){
        Inventario inventario = new Inventario(0);
        Item espada = new Item(1,"espada");
        inventario.adicionar(espada);
        assertEquals(espada,inventario.getItens().get(0));
    }
    
    @Test
    public void adicionarDoisItem(){
        Inventario inventario = new Inventario(0);
        Item espada = new Item(1,"espada");
        Item escudo = new Item(2,"escudo");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        assertEquals(espada,inventario.getItens().get(0));
        assertEquals(escudo,inventario.getItens().get(1));
    }
    
    @Test
    public void adicionarDoisItenComEspacoParaUmNapAdicionaSegundo(){
        Inventario inventario = new Inventario(4);
        Item espada = new Item(1,"espada");
        Item escudo = new Item(2,"escudo");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        assertEquals(espada,inventario.getItens().get(0));
        assertEquals(2,inventario.getItens().size());
    }
    
    @Test
    public void obterItemNaPrimeiraPosicao(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1,"espada");      
        inventario.adicionar(espada);
        assertEquals(espada,inventario.obter(0));
        
    }
    
    @Test
    public void obterItemNaoAdicionar(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1,"espada");      
        assertNull(inventario.obter(0));
        
    }
    
    @Test
    public void removerItem(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1,"espada"); 
        inventario.adicionar(espada);
        inventario.remover(espada);
        assertNull(inventario.obter(0));
    }
    
    @Test
    public void removerItemAntesDeAdicionarProximo(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1,"espada");
        Item escudo = new Item(1,"escudo");
        inventario.adicionar(espada);
        inventario.remover(espada);
        inventario.adicionar(escudo);
        assertEquals(escudo,inventario.obter(0));
        assertEquals(1,inventario.getItens().size());
    }
    
    @Test
    public void getDescricoesVariosItens(){
        Inventario inventario = new Inventario(4);
        Item espada = new Item(1,"espada");
        Item escudo = new Item(1,"escudo");
        Item armadura = new Item(1,"armadura");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(armadura);
        assertEquals("espada,escudo,armadura",inventario.getDescricoesItens());
    }
    
    @Test
    public void getDescricoesNenhumItem(){
        Inventario inventario = new Inventario(4);
        assertEquals("",inventario.getDescricoesItens());
    }
    
    @Test
    public void getItemMaiorQuantidadeComVarios(){
        Inventario inventario = new Inventario(4);
        Item espada = new Item(1,"espada");
        Item escudo = new Item(2,"escudo");
        Item armadura = new Item(3,"armadura");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(armadura);
        assertEquals(armadura,inventario.getItemComMaiorQuantidade());
    }
    
    @Test
    public void getItemMaiorQuantidadeInventarioVazio(){
        Inventario inventario = new Inventario(0);
        assertEquals(null,inventario.getItemComMaiorQuantidade());
    }
    
    @Test
    public void getItemMaiorQuantidadeComItensComMesmaQuantidade(){
        Inventario inventario = new Inventario(4);
        Item espada = new Item(1,"espada");
        Item escudo = new Item(1,"escudo");
        Item armadura = new Item(1,"armadura");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(armadura);
        assertEquals(espada,inventario.getItemComMaiorQuantidade());
    }
    
    @Test
    public void buscarItemComInventarioVazio(){
        Inventario inventario = new Inventario(0);
        assertNull(inventario.Buscar("Capa"));
    }
    
    @Test
    public void buscarItemComInventarioComMesmaDescricao(){
        Inventario inventario = new Inventario(1);
        Item termica1 = new Item(1, "Térmica");
        Item termica2 = new Item(1, "Térmica");
        inventario.adicionar(termica1);
        inventario.adicionar(termica2);
        assertEquals(termica1,inventario.Buscar("Térmica"));
    }
    
    @Test
    public void inverterInventarioVazio(){
        Inventario inventario = new Inventario(0);
        assertTrue(inventario.inverter().isEmpty());
    }
    
    @Test
    public void inverterComApenasUmItem(){
        Inventario inventario = new Inventario(1);
        Item termica = new Item(1, "Térmica");
        inventario.adicionar(termica);
        assertEquals(termica,inventario.inverter().get(0));
        assertEquals(1,inventario.inverter().size());
    }
    
    @Test
    public void inverterComDoisItens(){
        Inventario inventario = new Inventario(2);
        Item termica = new Item(1, "Térmica");
        Item caneca = new Item(1, "caneca");
        inventario.adicionar(termica);
        inventario.adicionar(caneca);
        assertEquals(caneca,inventario.inverter().get(0));
        assertEquals(2,inventario.inverter().size());
    }
}
